<?php
Namespace dgifford\WP_Plugin;



/*
	Class for adding Wordpress shortcodes.

	Extend this class and add shortcode/ method pairs to the shortcodes array. 

 

    Copyright (C) 2016  Dan Gifford

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */




class WP_Shortcodes extends WP_Entity_Abstract
{
	// Array of shortcodes and their default attributes
	protected $shortcodes = [];

	// Array of attributes provided for a shortcode
	protected $attributes = [];

	// Content from a shortcode
	protected $content = '';

	// The plugin object
	protected $plugin;

	// Holds array of settings from wp_options
	protected $options = [];






	public function __construct( WP_Plugin $plugin, $options )
	{
		/*
			Set properties
		 */
		$this->plugin = $plugin;
		$this->options = $options;



		/*
			Initialise
		 */
		$this->init();
	}



	/**
	 * Initialises shortcodes. Called at end of constructor.
	 * 
	 * @return null
	 */
	public function init()
	{
		foreach( $this->shortcodes as $key => $value )
		{
			if( is_int( $key ) )
			{
				$name = $value;
			}
			else
			{
				$name = $key;
			}

			add_shortcode( $name, [ $this, $name ] );
		}
	}



	/**
	 * Parses attributes and content into object properties.
	 * Sets any default attributes.
	 * Returns false if 'required' attributes are null or ''
	 * 
	 * @param  array 	$attributes
	 * @param  string 	$content 	
	 * @param  string 	$tag 
	 */
	protected function parse( $attributes = [], $content = '', $tag )
	{
		$this->attributes = shortcode_atts( 
			$this->getDefaultAttributes( $tag ), 
			$attributes, 
			$tag
		);

		$this->content = $content;

		$required = $this->getRequiredAttributes( $tag );

		foreach( $this->attributes as $key => $value )
		{
			if( in_array( $key, $required ) and ( is_null($value) or $value == '' ) )
			{
				return false;
			}
		}

		return true;
	}



	public function getDefaultAttributes( $tag = '' )
	{
		if( isset( $this->shortcodes[ $tag ]['defaults'] ) )
		{
			return $this->shortcodes[ $tag ]['defaults'];
		}

		return [];
	}



	public function getRequiredAttributes( $tag = '' )
	{
		if( isset( $this->shortcodes[ $tag ]['required'] ) )
		{
			return $this->shortcodes[ $tag ]['required'];
		}

		return [];
	}

}

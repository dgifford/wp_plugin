<?php
Namespace dgifford\WP_Plugin;



/**
	Abstract class that provides methods and structure for classes 
	adding Wordpress entities such as Post Types, Taxonomies and 
	shortcodes.

	Copyright (C) 2017  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */



Use dgifford\Filter\Sanitizer;
Use dgifford\Filter\Validator;
Use dgifford\Traits\PropertySetterTrait;



abstract class WP_Entity_Abstract
{
	Use PropertySetterTrait;



	// Container for entity definitions
	public $container = [];

	// dgifford\Filter\Sanitizer object
	protected $sanitizer;

	// Sanitization filters
	protected $sanitizer_filters;

	// dgifford\Filter\Validator object
	protected $validator;

	// Validation filters
	protected $validator_filters;

	// The plugin object
	protected $plugin;

	// The options object
	protected $options;








	/**
	 * Constructor requires the plugin object, an options object.
	 * 
	 * @param WP_Plugin            $plugin
	 * @param Options_Interface $options
	 */
	public function __construct( WP_Plugin $plugin, Options_Interface $options, $properties = [] )
	{
		/*
			Dependencies
		 */
		$this->plugin = $plugin;
		$this->options = $options;



		/*
			Create dgifford\sanitizer and validator objects
		 */
		$this->sanitizer = new Sanitizer;
		$this->validator = new Validator;



		/*
			Set any public properties
		 */
		$this->setPublicProperties( $properties, true );



		/*
			Calls all automatic setting methods
		 */
		$this->callAllSetters();



		/*
			Initialise
		 */
		$this->init();



		/*
			Register entity
		 */
		$this->register();



		/*
			Add hooks into WP
		 */
		$this->hooks();
	}



	/**
	 * Used by extending classes to set up entities.
	 * Called in constructor.
	 * 
	 * @return null
	 */
	public function init() {}



	/**
	 * Register the entity with WP.
	 * Called in constructor.
	 * 
	 * @return null
	 */
	public function register() {}



	/**
	 * Add any WP hooks required by entity
	 * Called in constructor.
	 * 
	 * @return null
	 */
	public function hooks() {}



	/**
	 * Validate the properties of an entity.
	 * 
	 * @param  string $name
	 * @param  array $properties
	 * @return array
	 */
	abstract protected function validateProperties( $name, $properties );



	/**
	 * Add a WP entity.
	 *  
	 * @param string $name 			The name of the entity.
	 * @param array  $properties 	Properties of the entity
	 */
	public function add( $name = '', $properties = [] )
	{
		$this->validator->set( $name, $this->validator_filters );

		if( !$this->validator->isValid() )
		{
			throw new \InvalidArgumentException( "Entity name '{$name}' invalid.");
		}

		$this->sanitizer->set( $name, $this->sanitizer_filters );

		if( !$this->plugin->isReservedTerm( $this->sanitizer->result ) )
		{
			$this->container[ $name ] = $this->validateProperties( $name, $properties );
		}
		else
		{
			throw new \InvalidArgumentException( "Entity name '{$name}' is a Wordpress reserved term.");
		}	
	}
}

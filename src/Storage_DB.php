<?php
Namespace dgifford\WP_Plugin;

/*
	Stores key/ value pairs in the WP Options table.


    Copyright (C) 2016  Dan Gifford

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */




class Storage_DB extends Plugin_Entity_Abstract implements Storage_Interface
{
	// Default values
	public $defaults = [];






	/**
	 * Set default(s)
	 * 
	 * Accepts a name/ value pair or an array of defaults.
	 */
	public function default()
	{
		$args = func_get_args();

		$options = [];

		// Array provided, set all defaults
		if( count($args) == 1 and is_array( $args[0] ) )
		{
			$this->defaults = $args[0];
		}
		// Key provided, set default to null
		elseif( count($args) == 1 and is_string( $args[0] ) )
		{
			$this->defaults[ $args[0] ] = null;
		}
		// Key/ value pair provided, set key
		elseif( count($args) == 2 )
		{
			$this->defaults[ $args[0] ] = $args[1];
		}
	}



	/**
	 * Clear defaults
	 * 
	 */
	public function clearDefaults()
	{
		$this->defaults = [];
	}



	/**
	 * Boolean test for if a default is set.
	 * 
	 * @param  string  $key
	 * @return boolean
	 */
	public function hasDefault( $key )
	{
		if( !isset( $this->defaults[ $key ]) or is_null( $this->defaults[ $key ]) )
		{
			return false;
		}

		return true;
	}



	/**
	 * Set key(s) with a value.
	 * 
	 * Accepts a key/ value pair or an array of pairs.
	 */
	public function set()
	{
		$args = func_get_args();

		$options = [];

		if( count($args) == 1 and is_array( $args[0] ) )
		{
			$options = $args[0];
		}
		elseif( count($args) == 1 and is_string( $args[0] ) )
		{
			$options = [ $args[0] => null ];
		}
		elseif( count($args) == 2 )
		{
			$options = [ $args[0] => $args[1] ];
		}

		foreach( $options as $key => $value )
		{
			update_option( $this->plugin->prefix( $key ), $value );
		}
	}



	/**
	 * Get the value of a key. If it is null, return the default
	 * @param  string $key
	 * @return mixed 		The value
	 */
	public function get( $key )
	{
		// Create Default
		$default = null;

		if( isset( $this->defaults[ $key ] ) )
		{
			$default = $this->defaults[ $key ];
		}

		// WP function
		return get_option( $this->plugin->prefix( $key ), $default );
	}



	/**
	 * Checks if an key exists.
	 * @param  string $key
	 * @return bool
	 */
	public function exists( $key )
	{
		if( $this->get( $key ) !== null )
		{
			return true;
		}

		return false;
	}



	/**
	 * Delete a key.
	 * @param  string $key
	 * @return bool 	 	Success
	 */
	public function delete( $key )
	{
		return delete_option( $this->plugin->prefix( $key ) );
	}



	/**
	 * Deletes all keys.
	 * @return Null
	 */
	public function clear()
	{
		// Delete options in DB
		$options = $this->keys();

		foreach( $options as $key )
		{
			$this->delete( $key );
		}

		$this->clearDefaults();
	}



	/**
	 * Returns a count of all keys set including defaults.
	 * 
	 * @return int
	 */
	public function count()
	{
		return count( $this->keys() );
	}



	/**
	 * Get an array of all keys and defaults.
	 * 
	 * @return array
	 */
	public function getAll()
	{
		$result = [];

		$options = $this->keys();

		foreach( $options as $name )
		{
			$result[ $name ] = $this->get( $name );
		}

		return $result;
	}



	/**
	 * Return an array of all keys without the plugin prefix.
	 *
	 * Combines data keys and default keys
	 * 
	 * @return array
	 */
	public function keys()
	{
		// Get defaults keys first
		$keys = array_keys( $this->defaults );

		// Query database for matching options
		global $wpdb;

		$results = $wpdb->get_results( "SELECT `option_name` FROM `{$wpdb->base_prefix}options` WHERE `option_name` LIKE '{$this->plugin->prefix()}%';", ARRAY_A);

		// Remove prefix from options and ass to keys
		foreach( $results as $option )
		{
			$name = substr( $option['option_name'], strlen($this->plugin->prefix()));
			$keys[] = $name;
		}

		return array_unique( $keys );
	}



	/**
	 * The uninstall script for the options.
	 * 
	 * @return Null
	 */
	public function uninstall()
	{
		$result = [];

		$options = $this->keys();

		foreach( $options as $name )
		{
			$result[] = "delete_option( '{$this->plugin->prefix( $name )}' );";
		}

		return implode("\n", $result);
	}
}
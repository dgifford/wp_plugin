<?php
Namespace dgifford\WP_Plugin;

/*
	Class to create a Wordpress Settings page.

	Settings are stored in the storage property (which implements the Storage_Interface).




	Copyright (C) 2016  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



Use dgifford\html\Prefab;
Use dgifford\Tag\Tag;
Use dgifford\Traits\ArrayHelpersTrait;
Use dgifford\Filter\Sanitizer;
Use dgifford\Filter\Validator;
Use dgifford\Filter\Filter;



class Settings_Page extends Plugin_Entity_Abstract
{
	Use ArrayHelpersTrait;

	// The settings page title
	public $page_title;

	// The settings page menu title
	public $menu_title;

	// The WP capability required to access this page
	public $capability = 'manage_options';

	// The slug used in the URL of the page and the action hook
	public $menu_slug;

	// Array used to define items added to the HTML.
	public $items = [];

	// HTML object for the settings page.
	public $html;

	// Storage_Interface object used to store settings
	public $storage;

	// Name of default Storage object
	public $storage_name = 'settings_page';

	// Holds array of setting error messages
	protected $errors = [];

	// Hook for the page defined when add_options_page is called.
	protected $hook;

	// dgifford\Filter\Sanitizer object
	protected $sanitizer;

	// dgifford\Filter\Validator object
	public $validator;

	// Default form field error if one is not specifically defined for a field.
	//public $default_form_error = 'There is a problem with this field.';

	// Default form field required error if one is not specifically defined for a field.
	//public $default_form_required_error = 'This field is required.';
	







	function init()
	{
		// Set a default activation message
		$this->plugin->setActivationMessage( 'Visit the <a href="' . $this->getURL() . '">' . $this->plugin->name() . ' settings page</a> to set the options for the plugin.' );

		// Hook into WP 
		$this->addHooks();
	}



	/**
	 * Hooks called in constructor.
	 * 
	 * @return null
	 */
	public function addHooks()
	{
		// The admin page
		add_action( 'admin_menu', [ $this, 'addSettingsPage' ] );

		// Processes the form input
		add_action( 'admin_post_' . $this->menu_slug, [ $this, 'processInput' ] );

		// Notices after save redirect
		add_action( 'admin_init', [ $this, 'saveNotices' ] );
	}






	///////////////////////////////////////////////
	// Methods hooked into WP
	///////////////////////////////////////////////



	/**
	 * Registers the settings page by hooking onto admin_menu action.
	 */
	public function addSettingsPage()
	{
		$this->hook = add_options_page(
			$this->page_title, 
			$this->menu_title, 
			$this->capability, 
			$this->menu_slug, 
			[$this, 'render' ]
		);
	}



	/**
	 * Processes the settings form input.
	 * 
	 * @return null
	 */
	public function processInput()
	{
		if( !empty( $_POST ) and check_admin_referer( $this->menu_slug ) )
		//if( !empty( $_POST ) )
		{
			foreach( $this->storage->keys() as $name )
			{
				// Sanitize
				$value = $this->sanitize( $name, $_POST[ $name ] );

				// Validate
				if( $this->validate( $name, $value ) )
				{
					$this->storage->set( $name, $value );
				}
			}

			wp_redirect( $this->getRedirectURL() );

			die;
		}
	}



	public function saveNotices()
	{
		if( $this->isSaved() )
		{
			Notice::success( $this->plugin->slug(), 'Settings Saved.' );
		}

		if( $this->hasErrors() )
		{
			$lines = [];

			foreach( $this->getErrors() as $field => $errors )
			{
				$lines = array_merge( $lines, $errors );
			}

			Notice::error( $this->plugin->slug(), $lines );

			$this->clearErrors();
		}		
	}








	///////////////////////////////////////////////
	// Sanitization and validation
	///////////////////////////////////////////////



	/**
	 * Returns the type of the field control or an
	 * empty string if not found.
	 * 
	 * @param  string $name  Field name
	 * @return string
	 */
	public function getFieldType( $name )
	{
		$field =  $this->getFieldControl( $name );

		switch( $field->getName() )
		{
			case 'textarea':
			case 'select':
				return $field->getName();
			break;

			case 'input':
				if( is_string( $field->getAttribute( 'type' ) ) )
				{
					return $field->getAttribute( 'type' );
				}
				else
				{
					return 'text';
				}
			break;
		}
		
		return '';
	}



	/**
	 * Check field control for required attribute.
	 * 
	 * @param  string $name  Field name
	 * @return bool
	 */
	public function isFieldRequired( $name )
	{
		$control = $this->getFieldControl( $name );

		if( $control !== false and $control->attributeEquals( 'required', true ) )
		{
			return true;
		}
		
		return false;
	}



	/**
	 * Sanitize the POST value with the key 'name'.
	 * Calls the first method found based on:
	 *  - field name
	 *  - field type
	 *  - default
	 * 
	 * @param  string $name  Field name
	 * @return bool
	 */
	public function sanitize( $name, $value )
	{
		// Field specific sanitization method
		if( ($result = $this->callIfMethodExists( "sanitize_{$name}", $value, null )) !== null )
		{
			return $result;
		}

		// Default sanitization based on field attributes
		$this->sanitizer->setValue( $value );

		// Get attributes
		$control = $this->getFieldControl( $name );

		if( $control !== false )
		{
			$attributes = $control->getAttributes( false );
		}
		else
		{
			$attributes = [];
		}

		// Use control attributes to validate
		if( is_array($attributes) )
		{
			$this->sanitizer->setFiltersFromAttributes( $attributes );
		}

		// Validate as string if no others
		if( empty($this->sanitizer->filters) )
		{
			$this->sanitizer->setFilters( 'string' );
		}		

		return $this->sanitizer->result();
	}



	/**
	 * Call a specific validation method for the name of the field
	 * otherwise validate based on the attributes of the field control.
	 * 
	 * @param  string $name  Field name
	 * @param  mixed $value  Value to validate
	 * @return bool
	 */
	public function validate( $name, $value )
	{
		// Field is required but empty
		if( $this->isFieldRequired( $name ) and $value == '' )
		{
			$this->addError( $name, $this->getRequiredErrorMessage( $name ) );

			return false;
		}

		// Try field specific validation method
		if( ($result = $this->callIfMethodExists( "validate_{$name}", $value, null )) === null )
		{
			// Validate as string if no filters set
			if( empty($this->sanitizer->filters) )
			{
				$this->validator->setFilters( 'is_string' );
			}

			$result = $this->sanitizer->isValid();
		}

		// Set error message
		if( $result == false )
		{
			$this->addError( $name, $this->getFieldErrorMessage( $name ) );
		}

		// Return validation of sanitized value
		return $result;
	}







	///////////////////////////////////////////////
	// Rendering
	///////////////////////////////////////////////



	/**
	 * Renders the settings page.
	 * 
	 * @return null
	 */
	public function render( $output = true )
	{	
		// Get the HTML object
		$html = $this->getHTML();

		// Apply filters to the html Tag object
		$html = apply_filters(
			$this->plugin->prefix('settings_page_html'), 
			$html
		);

		if( $output === false )
		{
			return $html->render( false );
		}
	
		$html->render();
	}







	///////////////////////////////////////////////
	// Setters for properties called automatically 
	// by callAllSetters trait method.
	///////////////////////////////////////////////



	/**
	 * Set the menu slug for the settings page.
	 * Defaults to the plugin slug
	 * @param string $menu_slug 
	 */
	public function set_menu_slug( $menu_slug = '' )
	{
		if( empty( $menu_slug ) )
		{
			$menu_slug = $this->plugin->slug();
		}

		$this->menu_slug = $menu_slug;
	}



	/**
	 * Set the settings page title.
	 * Defaults to the plugin name + ' Settings'
	 * @param string $page_title  	A string
	 */
	public function set_page_title( $page_title = '' )
	{
		if( empty( $page_title ) )
		{
			$page_title = $this->plugin->name() . ' Settings';
		}

		$this->page_title = $page_title;
	}



	/**
	 * Set the settings menu title.
	 * Defaults to the plugin name
	 * @param string $page_title  	A string
	 */
	public function set_menu_title( $menu_title = '' )
	{
		if( empty( $menu_title ) )
		{
			$this->menu_title = $this->plugin->name();
		}
	}



	/**
	 * Sets the WP capability required to access this menu.
	 * Defaults to manage_options.
	 * 
	 * @param string $capability
	 */
	public function set_capability( $capability = '' )
	{
		if( empty( $capability ) )
		{
			$this->capability = 'manage_options';
		}
	}



	/**
	 * The name of the storage object that holds the setttings
	 * 
	 * @param string $storage_name
	 */
	public function set_storage_name( $storage_name = '' )
	{
		if( is_string( $storage_name ) and !empty( $storage_name ) )
		{
			$this->storage_name = $storage_name;
		}
	}



	/**
	 * The Storage object used to persist settings.
	 * 
	 * @param string $storage
	 */
	public function set_storage( $storage = '' )
	{
		if( !$storage instanceof Storage_Interface )
		{
			$storage = new Storage_Array( $this->plugin, ['name' => $this->storage_name ] );
		}

		$this->storage = $storage;
	}



	/**
	 * Sets the html object for the page or generates the
	 * default page.
	 * 
	 * @param Tag $html
	 */
	public function set_html( $html = null )
	{
		if( !($html instanceof Tag) )
		{
			// Default HTML
			$html = Prefab::div()
				->addClass( 'wrap ' . $this->plugin->slug() )
				->add( Prefab::h1( $this->page_title ) )
				->add( Prefab::form('settings') )
			;
		}

		$this->html = $html;
	}



	/**
	 * Set the Sanitizer object.
	 * @param Sanitizer $sanitizer
	 */
	public function set_sanitizer( $sanitizer = null )
	{
		if( !($sanitizer instanceof Sanitizer) )
		{
			$sanitizer = New Sanitizer;
		}

		$this->sanitizer = $sanitizer;
	}



	/**
	 * Sets the Validator object
	 * @param Validator $validator
	 */
	public function set_validator( $validator = null )
	{
		if( !($validator instanceof Validator) )
		{
			$validator = New Validator;
		}

		$this->validator = $validator;
	}







	///////////////////////////////////////////////
	// Error handling
	///////////////////////////////////////////////



	public function clearErrors()
	{
		delete_transient( $this->getErrorsTransientKey() );
	}



	public function getErrors()
	{		
		$errors = get_transient( $this->getErrorsTransientKey() );

		if( is_array( $errors ) )
		{
			return $errors;
		}

		return [];
	}



	public function addError( $name, $message )
	{
		// Validate error info 
		if( empty( $name ) or !is_string($name) or empty( $message ) or !is_string($message)  )
		{
			return;
		}

		$errors = $this->getErrors();

		if( !isset( $errors[ $name ] )  )
		{
			$errors[ $name ] = [];
		}

		$errors[ $name ][] = $message;

		set_transient( $this->getErrorsTransientKey(), $errors, 2 * MINUTE_IN_SECONDS );
	}



	/**
	 * Returns the key identifying the transient used to store errors.
	 * Uniqiue to the user.
	 * 
	 * @return string
	 */
	public function getErrorsTransientKey()
	{
		return $this->menu_slug . '_' . get_current_user_id() . '_errors';
	}



	public function getError( $name = null )
	{
		$errors = $this->getErrors();

		if( !empty($name) and isset( $errors[$name] ) )
		{
			return $errors[$name];
		}

		return '';
	}



	public function hasErrors()
	{
		return !empty( $this->getErrors() );
	}







	///////////////////////////////////////////////
	// Adding a Setting
	///////////////////////////////////////////////
	


	/**
	 * Adds a field to the settings page. The field can be defined by
	 * a HTML tag object, or using type, name and property arguments.
	 *
	 * If the type, name and property arguments are used, the field
	 * will ba aded using standard WP settings page HTML and a storage
	 * element added if required.
	 *
	 * Also accepts an array of field arrays to add multiple Fields.
	 * 
	 * @param string $type       Field type, defaults to 'text'
	 * @param string $name       Field name, used as label and for name attribute.
	 * @param array  $properties Additional properties including input field attributes
	 */
	public function add( $type = '', $name = '', $properties = [] )
	{
		$args = func_get_args();

		// Check for a single Tag object
		if( count( $args ) == 1 and $args[0] instanceof Tag )
		{
			$this->html->filter('table', ['class'=>'form-table'])->add( $args[0] );
		}
		// Check for an array of Fields
		elseif( count( $args ) == 1 and is_array( $args[0] ) )
		{
			foreach( $args[0] as $arg )
			{
				call_user_func_array( [$this, 'add'], $arg );
			}
		}
		// Field definition array requires at least a string
		elseif( is_string( $args[0] ) )
		{
			// Get arguments merged with defaults
			extract( call_user_func_array([$this, 'getFieldArguments'], $args) );
		
			// Ensure an input name is included
			if( !empty( $attributes['name'] ) )
			{
				// Create storage key default
				if( !is_null( $default ) )
				{
					$this->storage->default( $attributes['name'], $default );
				}

				// Add the control to the form
				$this->addFieldHTML( $type, $label, $attributes, $description, $children );
			}
		}
	}



	public function addFieldHTML( $type, $label, $attributes, $description = '', $children = [] )
	{
		// Create the field row
		$row = Prefab::field('row');

		// Add the field label
		$row->filter('th')->add( Prefab::Label( $label )->setAttribute( 'for', $attributes['id'] ) );

		// Create control - first elemwnt in content array
		$content = [ Prefab::control( $type )->mergeAttributes( $attributes ) ];

		// Add children for controls with repeating data, e.g. select
		if( !empty($children) )
		{			
			$content[0]->processRepeatData($children);
		}

		// Add the field description - second element in content array
		if( !empty( $description ) )
		{
			$description_id = $content[0]->getAttribute('name') . '-description';

			$content[0]->setAttribute( 'aria-describedby', $description_id );

			$content[] = Prefab::control('description')->setAttribute( 'id', $description_id )->setContent( $description );
		}
	
		// Add the field contents
		$row->filter('td')->add( $content );

		// Add row to form
		$this->html->filter('table', ['class'=>'form-table'])->add( $row );
	}












	///////////////////////////////////////////////
	// Getters
	///////////////////////////////////////////////



	/**
	 * Returns the field control Tag object given the
	 * name attribute.
	 *
	 * Returns false if not found.
	 * 
	 * @param  string $name
	 * @return Tag object/ bool
	 */
	public function getFieldControl( $name )
	{
		$fields = $this->html->find('', ['name' => $name]);

		if( isset($fields[0]) and $fields[0] instanceof Tag )
		{
			return $fields[0];
		}

		return false;
	}



	public function isSaved()
	{
		if( isset($_GET['status']) )
		{
			return true;
		}

		return false;
	}


	/**
	 * Return the redirect URL after form submission.
	 * 
	 * @param  boolean $status [description]
	 * @return [type]          [description]
	 */
	public function getRedirectURL()
	{
		if( !empty($_POST[ '_wp_http_referer' ]) )
		{
			$redirect_url = $_POST[ '_wp_http_referer' ];
		}
		else
		{
			$redirect_url = $this->getURL();	
		}

		if( $this->hasErrors() )
		{
			$redirect_url = add_query_arg( 'status', 'errors', $redirect_url );
		}
		else
		{
			$redirect_url = add_query_arg( 'status', 'success', $redirect_url );
		}

		return $redirect_url;
	}


	/**
	 * Returns the URL of the settings page
	 * @return string URL
	 */
	public function getURL() 
	{
		return admin_url( 'options-general.php?page=' . $this->menu_slug ); 
	}



	/**
	 * Called by render method. Modifies the HTML object 
	 * just before rendering.
	 *
	 * Adds the nonce, action and referrer hidden fields.
	 *
	 * Adds default fields for any storage keys that don't
	 * have existing fields.
	 * 
	 * @return HTML
	 */
	public function getHTML()
	{
		// Get current settings values
		$values = $this->storage->getAll();

		// Add HTML for fields in storage that aren't in HTML object
		foreach( $values as $key => $value )
		{
			$result = $this->html->find('', ['name' => Filter::sanitize( $key, 'name_attribute' ) ]);

			// No field matching $key as a name attribute, so add default text field
			if( empty( $result ) )
			{
				$this->add( $key );
			}
		}

		// Add WP form field values
		$values[ 'action' ] = $this->menu_slug;
		$values[ '_wpnonce' ] = wp_create_nonce( $this->menu_slug );

		// Set form values
		$this->html->filter('form', ['action' => 'admin-post.php' ])->setValues( $values );

		// Add the referrer
		if( !empty( $_SERVER['REQUEST_URI']) )
		{
			$this->html->filter('form')->prepend(
				Prefab::Hidden()->mergeAttributes([ 'name' => '_wp_http_referer', 'value' => $_SERVER['REQUEST_URI'],]) 
			);
		}

		return $this->html;
	}



	/**
	 * Return the arguments required to create a field.
	 * 
	 * @return array
	 */
	public function getFieldArguments()
	{
		$args = func_get_args();

		$type = 'text';
		$name = '';
		$label = '';
		$description = '';
		$default = null;
		$attributes = [];
		$children = [];

		switch( count( $args ) )
		{
			case 1:
				// Single string field provided, use as label and name, default 'text' type
				if( is_string( $args[0] ) )
				{
					$label = $args[0];
					$name = $args[0];
				}
			break;


			case 2:
				// Name and properties provided, keep default 'text' type
				if( is_string( $args[0] ) and is_array( $args[1] ) )
				{
					$label = $args[0];
					$name = $args[0];
					extract( $args[1] );
				}
				// Type and label strings provided
				elseif( is_string( $args[0] ) and is_string( $args[1] ) )
				{
					$type = $args[0];
					$label = $args[1];
					$name = $args[1];
				}
			break;


			case 3:
				// Type, label and properties provided
				if( is_string( $args[0] ) and is_string( $args[1] ) and is_array( $args[2] ) )
				{
					$type = $args[0];
					$label = $args[1];
					$name = $args[1];
					extract( $args[2] );
				}
				// Type, label and default value provided
				elseif( is_string( $args[0] ) and is_string( $args[1] ) and !is_array( $args[2] ) )
				{
					$type = $args[0];
					$label = $args[1];
					$name = $args[1];
					$default = $args[2];
				}
			break;
		}

		// Create name attribute
		if( !isset( $attributes['name'] ) )
		{
			$attributes['name'] = Filter::sanitize( $name, 'name_attribute' );
		}

		// Create id attribute
		if( !isset( $attributes['id'] ) )
		{
			$attributes['id'] = $attributes['name'] . '-id';
		}
		
		return compact( 'type', 'label', 'description', 'default', 'attributes', 'children' );
	}



	public function getRequiredErrorMessage( $name = '' )
	{
		return "The field '{$name}' is required.";
	}



	public function getFieldErrorMessage( $name = '' )
	{
		return 'This field ({$name}) has an error.';
	}
}
<?php
Namespace dgifford\html;

return new Form([ 'method' => 'POST', 'action' => 'admin-post.php', ],
[
	Prefab::Hidden()->mergeAttributes([ 'name' => 'action', ]),

	Prefab::Hidden()->mergeAttributes([ 'id' => '_wpnonce', 'name' => '_wpnonce', ]),

	Prefab::Table()->addClass('form-table'),

	Prefab::Submit()->mergeAttributes([ 'id' => 'submit', 'name' => 'submit', 'class' => 'button button-primary', ])->setContent('Save Settings'),
]);
<?php
Namespace dgifford\WP_Plugin;

/*
	Class to create a Wordpress Settings page.
	Settings are stored in a single array.


	Copyright (C) 2016  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


Use DMG\WP_Plugin\WP_Plugin;
Use DMG\hf\hf;
Use DMG\wpf\wpf;
Use dgifford\Filter\Sanitizer;
Use dgifford\Filter\Validator;
Use dgifford\Traits\PropertySetterTrait;
Use dgifford\Traits\ArrayHelpersTrait;
Use dgifford\Tag\Tag;


class WP_Settings_Page
{
	Use PropertySetterTrait;
	Use ArrayHelpersTrait;

	// The plugin object
	public $plugin;

	// The settings page title
	public $page_title;

	// The settings page menu title
	public $menu_title;

	// The WP capability required to access this page
	public $capability = 'manage_options';

	// The slug used in the URL of the page
	public $menu_slug;

	// The name of the field in wp_options used to store setttings
	public $settings_name;

	// Holds array of settings from wp_options
	protected $options = [];

	// Holds array of setting error messages
	protected $errors = [];

	// Array holding the content of the settings page.
	// Each item is an array with keys 'type' and 'data'.
	protected $content = [];

	// Hook for the page defined when add_options_page is called.
	protected $hook;

	// dgifford\Filter\Sanitizer object
	protected $sanitizer;

	// dgifford\Filter\Validator object
	protected $validator;

	// Default form field error if one is not specifically defined for a field.
	public $default_form_error = 'There is a problem with this field.';

	// Default form field required error if one is not specifically defined for a field.
	public $default_form_required_error = 'This field is required.';







	///////////////////////////////////////////////
	// Initiation
	///////////////////////////////////////////////



	/**
	 * 	Loads plugin object and page properties
	 * 	Calls addContent method to create the page
	 * 	Registers hooks
	 * 	Initialises sanitization and validation objects
	 * 
	 * @param WP_Plugin $plugin DMG\WP_Plugin object
	 * @param array     $p      Array of public properties
	 */
	public function __construct( WP_Plugin $plugin, Options_Interface $options, $p = [] )
	{
		/*
			Set properties
		 */
		$this->plugin = $plugin;
		$this->options = $options;
		$this->setPublicProperties( $p, true );



		/*
			Create dgifford\sanitizer and validator objects
		 */
		$this->sanitizer = new Sanitizer;
		$this->validator = new Validator;



		/*
			Calls all automatic setting methods
		 */
		$this->callAllSetters();



		/*
			Initialise page - this method is used by extending classes.
		 */
		$this->init();



		/*
			Wordpress hooks
		 */
		
		// The admin page
		add_action( 'admin_menu', [ $this, 'addSettingsPage' ] );

		// Processes the form input
		add_action( 'admin_post_' . $this->settings_name, [ $this, 'processInput' ] );

		// Notices on save
		$this->saveSuccessNotices();

		// Notices on plugin activation
		add_action( 'plugins_loaded', [ $this, 'activationNotices' ]);
	}



	/**
	 * Called at the start of the constructor. Replace in child classes to add content.
	 * 
	 * @return null
	 */
	public function init()
	{
	}





	///////////////////////////////////////////////
	// Methods hooked into WP
	///////////////////////////////////////////////



	/**
	 * Registers the settings page by hooking onto admin_menu action.
	 */
	public function addSettingsPage()
	{
		$this->hook = add_options_page( $this->page_title, $this->menu_title, $this->capability, $this->menu_slug, [$this, 'render' ] );
	}



	public function processInput()
	{
		$errors = [];

		if( !empty( $_POST ) && check_admin_referer( $this->settings_name, '_wpnonce' ) )
		{
			foreach( $this->content as $index => $content )
			{
				if( $content['type'] == 'setting' )
				{
					extract( $content['data'] );

					// Check if empty and required
					if(
						$this->getRequired( $properties ) === true 
						and 
						(!isset($_POST[ $this->getFieldName( $properties ) ]) or $_POST[ $this->getFieldName( $properties ) ] == '' )
					)
					{
						// Set error and get default value
						$errors[ $this->getFieldName( $properties ) ] = $this->getRequiredErrorMessage( $properties );
						$this->options->set( $this->getFieldName( $properties ) );
					}
					else
					{
						$sanitize_method = 'sanitize_' . $this->getFieldName( $properties );

						// Sanitize the input
						$sanitize_method = $this->getFirstCallable([ 
							'sanitize_' . $this->getFieldName( $properties ), 
							'sanitize_' . $properties['type'], 
							'sanitize',
						]);

						if( isset( $_POST[ $this->getFieldName( $properties ) ] ) )
						{
							$value = call_user_func( $sanitize_method, $_POST[ $this->getFieldName( $properties ) ], $properties );
						}
						else
						{
							$value = '';
						}


						if( $value !== '' )
						{
							// Validate the input
							$validate_method = $this->getFirstCallable([ 
								'validate_' . $this->getFieldName( $properties ), 
								'validate_' . $properties['type'], 
								'validate',
							]);

							if( call_user_func( $validate_method, $value, $properties ) )
							{
								$this->options->set( $this->getFieldName( $properties ), $value );
							}
							else
							{
								$errors[ $this->getFieldName( $properties ) ] = $this->getFieldErrorMessage( $properties );
							}
						}
					}
					
				}
			}

			$redirect_url = $_POST[ '_wp_http_referer' ];

			// Store any errors
			if( !empty($errors) )
			{
				set_transient( $this->getFormErrorsTransientKey(), $errors, 10 * MINUTE_IN_SECONDS );
				$redirect_url = add_query_arg( 'status', 'errors', $redirect_url );
			}
			else
			{
				$redirect_url = add_query_arg( 'status', 'success', $redirect_url );
			}

			// Update options in WP
			$this->options->save();

			wp_redirect( $redirect_url );
			exit;
		}
	}



	public function saveSuccessNotices()
	{
		// Save success/ error admin notices
		if( isset($_GET['status']) and $_GET['status'] == 'success' )
		{
			WP_Admin_Notice::success( $this->plugin, 'Settings Saved.' );
		}
		elseif( isset($_GET['status']) and $_GET['status'] == 'errors' )
		{
			WP_Admin_Notice::error( $this->plugin, 'There is an error.' );
		}
	}



	public function activationNotices()
	{
		if( $this->plugin->getFlag( 'activated' ) )
		{
			WP_Admin_Notice::success( $this->plugin, 'Visit the <a href="' . $this->getURL() . '">' . $this->plugin->getName() . ' settings page</a> to set the options for the plugin.' );
		
			$this->plugin->deleteFlag( 'activated' );
		}
	}






	///////////////////////////////////////////////
	// Sanitization and validation
	///////////////////////////////////////////////



	/**
	 * Default sanitization method.
	 * Uses field control attributes.
	 */
	public function sanitize( $value, $properties )
	{
		$this->sanitizer->set( $value );

		$this->sanitizer->setFiltersFromAttributes( $properties['attributes'] );

		if( empty( $this->sanitizer->filters ) )
		{
			$this->sanitizer->setFilters( 'string' );
		}

		return $this->sanitizer->result;
	}



	/**
	 * Default validation method.
	 * Uses field control attributes.
	 */
	public function validate( $value, $properties )
	{
		$this->validator->set( $value );

		if( empty( $properties['attributes']['type']  ) )
		{
			$properties['attributes']['type']  = $properties['type'];
		}
		
		$this->validator->setFiltersFromAttributes( $properties['attributes'] );

		return $this->validator->isValid();
	}



	/**
	 * Default select field validation method.
	 */
	public function validate_select( $value, $properties )
	{
		if( is_array( $properties['options'] ) )
		{
			return $this->arrayInArray( $value, $properties['options'] );
		}
		elseif( $value == '' )
		{
			return true;
		}

		return false;
	}



	/**
	 * Default radio field validation method.
	 */
	public function validate_radio( $value, $properties )
	{
		return $this->validate_select( $value, $properties );
	}



	/**
	 * Default radios field validation method.
	 */
	public function validate_radios( $value, $properties )
	{
		return $this->validate_select( $value, $properties );
	}



	/**
	 * Default boolean field validation.
	 */
	public function validate_boolean( $value, $properties )
	{
		if( in_array( $value, [ '0', '1' ] ) )
		{
			return true;
		}

		return false;
	}



	/**
	 * Default boolean field validation.
	 */
	public function validate_checkboxes( $values, $properties )
	{
		// Checkbox group can be empty
		if( empty($values) )
		{
			return true;
		}

		if( is_array( $properties['options'] ) )
		{
			return $this->arrayInArray( $values, $properties['options'] );
		}

		return false;
	}



	/**
	 * Default checkbox field validation.
	 */
	public function validate_checkbox( $value, $properties )
	{
		// Checkbox can be empty
		if( $value == '' )
		{
			return true;
		}

		if(
			(isset($properties['attributes']['value']) and $value == $properties['attributes']['value'])
			or
			($value == $properties['label'])
		)
		{
			return true;
		}

		return false;
	}



	public function arrayInArray( $needles, $haystack )
	{
		$needles = $this->makeArray( $needles );

		foreach( $needles as $needle )
		{
			if( !in_array( $needle, $haystack ) )
			{
				return false;
			}
		}

		return true;
	}








	///////////////////////////////////////////////
	// Adding content
	// These methods should be called by child classes
	// within the addContent method.
	///////////////////////////////////////////////



	/**
	 * Adds a setting.
	 * 
	 * @param string $value [description]
	 */
	public function add( $name, $properties = [] )
	{
		$this->sanitizer->set( $name, 'name_attribute' );

		$defaults =
		[
			'label' 		=> $name,
			'type' 			=> 'text',	// Control/ field type
			'content' 		=> '', 		// Content between non-void elements
			'default' 		=> '',		// Default value as string
			'options' 		=> [],		// Values for select/ checkbox/ radio groups
			'required_error'=> '',		// Values for select/ checkbox/ radio groups
			'error'			=> '',		// Values for select/ checkbox/ radio groups
			'attributes' 	=>
			[
				'name' 	=> $this->sanitizer->result,
				'id' 	=> $this->sanitizer->result . '-id',
			],
		];

		$properties = array_replace_recursive( $defaults, $properties );

		// Copy properties that could be attributes
		foreach( $properties as $key => $value )
		{
			if( !isset( $properties['attributes'][$key] ) and !array_key_exists( $key, $defaults) )
			{
				$properties['attributes'][$key] = $value;
			}
		}

		// Set the default if it exists
		if( !is_null( $properties['default'] ) )
		{
			$this->options->setDefault( $this->getFieldName( $properties ), $properties['default'] );
		}

		$this->content[] = [
			'type' => 'setting',
			'data' => 
			[
				'name' => $name,
				'properties' => $properties,
			],
		];
	}



	/**
	 * Adds a section with a title and information.
	 * 
	 * @param string $title 	The section title
	 * @param string $info 		Information about the section
	 */
	public function addSection( $title, $info = '' )
	{
		$this->content[] = [
			'type' => 'section',
			'data' => 
			[
				'title' => $title,
				'info' => $info,
			],
		];
	}



	/**
	 * Adds HTML.
	 * 
	 * @param string $title 	The section title
	 * @param string $info 		Information about the section
	 */
	public function addHTML( $html )
	{
		$this->content[] = [
			'type' => 'html',
			'data' => $html,
		];
	}







	///////////////////////////////////////////////
	// Setters called automatically
	///////////////////////////////////////////////



	public function set_menu_slug( $menu_slug = '' )
	{
		if( empty( $menu_slug ) )
		{
			$this->menu_slug = $this->plugin->getSlug();
		}
	}



	/**
	 * Set the settings page title.
	 * Defaults to the plugin name + ' Settings'
	 * @param string $page_title  	A string
	 */
	public function set_page_title( $page_title = '' )
	{
		if( empty( $page_title ) )
		{
			$this->page_title = $this->plugin->getName() . ' Settings';
		}
	}



	/**
	 * Set the settings menu title.
	 * Defaults to the plugin name
	 * @param string $page_title  	A string
	 */
	public function set_menu_title( $menu_title = '' )
	{
		if( empty( $menu_title ) )
		{
			$this->menu_title = $this->plugin->getName();
		}
	}



	/**
	 * Sets the WP capability required to access this menu.
	 * Defaults to manage_options.
	 * 
	 * @param string $capability
	 */
	public function set_capability( $capability = '' )
	{
		if( empty( $capability ) )
		{
			$this->capability = 'manage_options';
		}
	}



	/**
	 * The name of the field in wp_options that holds the setttings for this page.
	 * Defaults to the plugin prefix without a trailing underscore.
	 * 
	 * @param string $settings_name
	 */
	public function set_settings_name( $settings_name = '' )
	{
		if( empty( $settings_name ) )
		{
			$this->settings_name = rtrim( $this->plugin->getPrefix(), '_' );
		}
	}



	public function set_errors( $errors = '' )
	{
		if( empty( $errors ) )
		{
			$this->errors = get_transient( $this->getFormErrorsTransientKey() );
			delete_transient( $this->getFormErrorsTransientKey() );

			if( $this->errors === false )
			{
				$this->errors = [];
			}
		}
		else
		{
			$this->errors = $errors;
		}
	}









	///////////////////////////////////////////////
	// Getters
	///////////////////////////////////////////////



	public function getURL() 
	{
		return admin_url( 'options-general.php?page=' . $this->menu_slug ); 
	}



	public function getLink() 
	{
		return '<a href="' . $this->getURL() . '">' . __( $this->page_title, $this->plugin->getTextDomain() ) . '</a>'; 
	}


	/**
	 * Returns true if current page is this settings page.
	 * @return bool
	 */
	public function onPage() 
	{ 
		if( isset($_GET['page']) and $_GET['page'] == $this->menu_slug ) 
		{ 
			return true; 
		} 

		return false; 
	}



	/**
	 * Returns the key identifying the transient used to store errors.
	 * Uniqiue to the user.
	 * 
	 * @return string
	 */
	public function getFormErrorsTransientKey()
	{
		return $this->menu_slug . get_current_user_id() . '_errors';
	}



	/**
	 * Returns true if a field is required.
	 * @param  array $properties Field properties
	 * @return string
	 */
	protected function getRequired( $properties )
	{
		if( isset($properties['attributes']['required']) )
		{
			return boolval( $properties['attributes']['required'] );
		}

		return false;
	}


	/**
	 * Returns the error specific to this field, or, if unavailable, a generic one.
	 * @param  array $properties Field properties
	 * @return string
	 */
	protected function getFieldErrorMessage( $properties )
	{
		if( !empty($properties['error']) )
		{
			return $properties['error'];
		}

		return $this->default_form_error;
	}



	/**
	 * Returns the required field error specific to this field, or, if unavailable, a generic one.
	 * @param  array $properties Field properties
	 * @return string
	 */
	protected function getRequiredErrorMessage( $properties )
	{
		if( !empty($properties['required_error']) )
		{
			return $properties['required_error'];
		}

		return $this->default_form_required_error;
	}



	/**
	 * Returns the default value from the given field properties.
	 * 
	 * @param  array $properties
	 * @return mixed
	 */
	public function getFieldName( $properties )
	{
		if( isset( $properties['attributes']['name'] ) )
		{
			return $properties['attributes']['name'];
		}

		return '';
	}



	/**
	 * Returns the fist callable method within this class from an array of method names.
	 * 
	 * @param  array  $methods [description]
	 * @return [type]          Callable
	 */
	protected function getFirstCallable( $methods = [] )
	{
		foreach( $methods as $method )
		{
			if( is_callable([ $this, $method ]) )
			{
				return [ $this, $method ];
			}
		}
	}



	/**
	 * Returns CSS class for the div that wraps the content, applying filters.
	 * 
	 * @return [type]
	 */
	public function getWrapperClass()
	{
		// Must include 'wrap' for admin notices to be placed correctly
		return 'wrap ' . apply_filters(
			$this->plugin->getPrefix() . 'settings_page_wrapper_class', 
			'dmg_plugin_settings ' . $this->settings_name
		);
	}



	public function getTitle()
	{
		$title = new Tag( 'h1', get_admin_page_title() );

		return apply_filters(
			$this->plugin->getPrefix() . 'settings_page_title', 
			$title->render( false )
		);
	}



	public function getSubmitButton( $value = '' )
	{
		$button = new Tag( 'p', ['class' => 'submit'] );

		$button->addContent( 'input', [
			'value' => 'Save Changes',
			'type' => 'submit',
			'id' => 'submit',
			'name' => 'submit',
			'class' => 'button button-primary',
		]);

		return $button;
	}



	/**
	 * Return the HTML for the settings form.
	 * 
	 * @param  string $value [description]
	 * @return [type]        [description]
	 */
	public function getForm()
	{
		$this->addFormTables();

		$form = new Tag( 'form', [ 'method' => 'POST', 'action' => 'admin-post.php', ]);

		$form->addContent( 'input', [ 'name' => 'action', 'value' => $this->settings_name, 'type' => 'hidden'] );

		$form->addContent( wp_nonce_field( $this->settings_name, '_wpnonce', true, false ), false );

		$sections = 0;

		foreach( $this->content as $index => $content )
		{
			$method = "get_{$content['type']}";

			$form->addContent( $this->$method( $content['data'] ) );
		}

		$form->addContent( $this->getSubmitButton() );

		return $form;
	}



	public function addFormTables()
	{
		$result = [];

		$table_open = false;

		foreach( $this->content as $index => $content )
		{
			// Open the form table
			if( $content['type'] == 'setting' and !$table_open )
			{
				$result[] = [ 'type' => 'html',	'data' => '<table class="form-table">',	];
				$result[] = $content;
				$table_open = true;
			}
			// Close the form table
			elseif( $content['type'] != 'setting' and $table_open )
			{
				$result[] = [ 'type' => 'html',	'data' => '</table>',	];
				$result[] = $content;
				$table_open = false;
			}
			else
			{
				$result[] = $content;
			}			
		}

		if( $table_open )
		{
			$result[] = [ 'type' => 'html',	'data' => '</table>',	];
		}

		$this->content = $result;
	}



	public function get_section( $data )
	{
		extract( $data );

		$title = new Tag( 'h2', $title );

		$info = new Tag( 'div', ['class' => 'info' ], $info );

		return [ [$title], [$info] ];
	}



	public function get_html( $data )
	{
		return [ [$data, false] ];
	}



	public function get_setting( $data )
	{
		extract( $data );

		$row = new Tag( 'tr' );

		// Generate label cell
		$label = new Tag( 'th', ['scope' => 'row' ] );
		$label->addContent( 'label', ['for' => $properties['attributes']['id'] ], $properties['label'] );
		
		// Generate field cell
		$field = new Tag( 'td' );

		if( is_callable([$this, 'getControl_' . $properties['type']]) )
		{
			$control = $this->{'getControl_' . $properties['type']}( $properties );
		}
		else
		{
			$control = $this->getControl_input( $properties );
		}

		$field->addContent( $control );

		$field->addContent( $this->getError( $properties ) );

		// Add content to the row
		$row->addContent([ [$label], [$field] ]);

		return $row;
	}



	protected function getError( $properties )
	{
		if( isset( $this->errors[ $this->getFieldName( $properties ) ] ) )
		{
			$error_tag = new Tag( 'span', ['class' => 'error'] );

			$errors = $this->makeArray( $this->errors[ $this->getFieldName( $properties ) ] );

			$error_tag->addContent( implode(' ', $errors ));

			return $error_tag;
		}

		return '';
	}



	public function getCurrentValue( $properties )
	{
		return $this->options->get( $this->getFieldName( $properties ) );
	}









	///////////////////////////////////////////////
	// Get form control HTML
	///////////////////////////////////////////////



	public function getControl_input( $properties )
	{
		extract( $properties );
		$attributes['type'] = $type;
		$attributes['value'] = $this->getCurrentValue( $properties );

		return (new Tag( 'input', $attributes ));
	}



	public function getControl_checkbox( $properties )
	{
		extract( $properties );

		$attributes['type'] = 'checkbox';

		if( !isset($attributes['value']) )
		{
			$attributes['value'] = $label;
		}

		if( $attributes['value'] == $this->getCurrentValue( $properties ) )
		{
			$attributes['checked'] = true;
		}

		return (new Tag( 'input', $attributes ));
	}



	public function getControl_boolean( $properties )
	{
		extract( $properties );

		$html = [];

		$html[] = [ new Tag( 'input', [ 'type' => 'hidden', 'name' => $attributes['name'], 'value' => 0] ) ];

		$attributes['type'] = 'checkbox';
		$attributes['value'] = 1;

		if( $this->getCurrentValue( $properties ) == 1 )
		{
			$attributes['checked'] = true;
		}

		$html[] = [ new Tag( 'input', $attributes ) ];

		return $html;
	}



	public function getControl_textarea( $properties )
	{
		extract( $properties );
		return new Tag( 'textarea', $attributes, $this->getCurrentValue( $properties ) );
	}



	public function getControl_select( $properties )
	{
		if( empty($properties['options']) ) {return;}

		$html = new Tag( 'select', $properties['attributes'] );

		foreach( $properties['options'] as $label => $value )
		{
			if( is_int( $label ) )
			{
				$label = $value;
			}

			$option_attributes = ['value' => $value];

			if( $this->getCurrentValue( $properties ) == $value )
			{
				$option_attributes['selected'] = true;
			}

			$html->addContent( 'option', $option_attributes, $label );
		}

		return $html;
	}



	public function getControl_checkboxes( $properties )
	{
		return $this->getControlInputList( 'checkbox', $properties );
	}



	public function getControl_radio( $properties )
	{
		return $this->getControlInputList( 'radio', $properties );
	}



	public function getControl_radios( $properties )
	{
		return $this->getControlInputList( 'radio', $properties );
	}



	public function getControlInputList( $type, $properties )
	{
		if( empty($properties['options']) ) {return;}

		$properties['attributes']['type'] = $type;

		if( $type == 'checkbox' )
		{
			$properties['attributes']['name'] = rtrim( $properties['attributes']['name'], '[]' ) . '[]';
		}

		$current = $this->makeArray( $this->getCurrentValue( $properties ) );

		$control = new Tag( 'div', ['class' => $type ] );

		$count = 0;

		foreach( $properties['options'] as $label => $value )
		{
			$item = new Tag( 'p', ['class' => 'item item_' . $count ] );

			if( is_int( $label ) )
			{
				$label = $value;
			}

			// Set item's attributes
			$item_attr = $properties['attributes'];
			$item_attr['id'] = "{$properties['attributes']['id']}_{$count}";
			$item_attr['value'] = $value;

			if( in_array( $value, $current ))
			{
				$item_attr['checked'] = true;
			}

			$item->addContent( 'input', $item_attr );
			$item->addContent( 'label', ['for' => $item_attr['id'] ], $label );

			$control->addContent( $item );

			$count++;
		}

		return $control;
	}







	///////////////////////////////////////////////
	// Rendering
	///////////////////////////////////////////////



	/**
	 * Renders the settings page.
	 * 
	 * @return null
	 */
	public function render()
	{
		// Get errors for this form
		$this->set_errors();

		// Start the html output
		$html = new Tag( 'div', ['class' => $this->getWrapperClass() ]);

		$html->addContent( $this->getTitle(), false );

		$html->addContent( $this->getForm() );

		// Apply filters to the html Tag object
		$html = apply_filters(
			$this->plugin->getPrefix() . 'settings_page_html', 
			$html
		);
	
		$html->render();
	}
}
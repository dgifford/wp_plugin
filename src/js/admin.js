jQuery(document).ready(function( $ )
{
	$( 'button.dmg-ebay-lister.refresh' ).on( 'click', function( event )
	{
		var data = {
			'action': 'dmg_ebay_lister_refresh',
			'template': $( '.acf-field[data-name="template"] select option:selected').val(),
			'post_ID': $( '#post input[name="post_ID"]').val()
		};

		//console.log( data );

		$.post( ajaxurl, data )
			.done(function( response )
			{
				response = JSON.parse( response );  
				console.log( response.args );
				$('.dmg-ebay-lister.preview' ).html( response.args );
			});

		

		event.preventDefault();
	});
});
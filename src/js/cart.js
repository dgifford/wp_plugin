jQuery(document).ready(function( $ )
{
	$( 'form.dmg-shopping-cart button' ).on( 'click', function( event )
	{
		var formData = $(this).closest('form.dmg-shopping-cart').serializeArray();

		formData.push({ name: this.name, value: this.value });

		formData.push({ name: 'ajax', value: 1 });

		console.log( formData );

		$.post( '', formData )
			.done(function( data )
			{
				cart = JSON.parse( data );
				
				$.each(cart.totals, function(key, value)
				{
					$('.dmg-shopping-cart.' + key ).text( value );
				});
				
				$.each(cart.items, function(key, obj)
				{
					var id = obj.id;
					delete obj.id;

					$.each(obj, function(index, value)
					{
						$( '.dmg-shopping-cart.items tr[data-product-id="'+id+'"] span.' + index ).text( value );
					});
;				});
			});

		event.preventDefault();
	});
});
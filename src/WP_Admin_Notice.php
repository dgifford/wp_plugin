<?php
Namespace dgifford\WP_Plugin;



/*
	Class to generate Wordpress admin notices.


    Copyright (C) 2016  Dan Gifford

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



Use dgifford\Traits\PropertySetterTrait;
Use dgifford\Tag\Tag;



class WP_Admin_Notice
{
	Use PropertySetterTrait;



	public $lines = [];

	public $type = 'info';
	
	public $dismissable = true;
	
	public $priority = 10;

	protected $types_allowed = [ 'error', 'warning', 'success', 'info', ];

	protected $default_class = [ 'notice', ];






	/////////////////////////////////////////////
	// Initiation
	/////////////////////////////////////////////



	public function __construct( WP_Plugin $plugin, $properties = [] )
	{
		/*
			Set dependency
		 */
		$this->plugin = $plugin;



		/*
			Set properties
		 */
		if( is_array( $properties ) )
		{
			$this->setPublicProperties( $properties );
		}
		elseif( $this->nonEmptyString( $properties ) )
		{
			$this->set( $properties );
		}



		/*
			Calls all automatic setting methods
		 */
		$this->callAllSetters();
	}



	public function addAction()
	{
		if( !empty( $this->lines ) )
		{
			add_action( 'admin_notices', [ $this, 'render' ], $this->priority );
		}
	}






	/////////////////////////////////////////////
	// Setters
	/////////////////////////////////////////////



	public function set( $message = '' )
	{
		if( $this->nonEmptyString( $message ) )
		{
			$this->lines = [ $message ];
		}
	}



	public function set_lines( $lines = [] )
	{
		if( is_array( $lines ) )
		{
			$this->lines = [];

			foreach( $lines as $line => $value )
			{
				if( $this->nonEmptyString( $value ) )
				{
					$this->lines[] = $value;
				}
			}
		}
		elseif( $this->nonEmptyString( $lines ) )
		{
			$this->lines = [ $lines ];
		}
	}



	public function add( $message = '' )
	{
		if( $this->nonEmptyString( $message ) )
		{
			$this->lines[] = $message;
		}
	}



	public function set_type( $type = '' )
	{
		if( $this->validType( $type ) )
		{
			$this->type = $type;
		}
	}



	public function set_dismissable( $dismissable = true )
	{
		$this->dismissable = boolval( $dismissable );
	}



	public function set_priority( $priority = 10 )
	{
		$this->priority = intval( $priority );
	}



	public function addClass( $class = '' )
	{
		if( $this-nonEmptyString( $class ) )
		{
			$this->classes[] = $class;
		}
	}





	/////////////////////////////////////////////
	// Validation
	/////////////////////////////////////////////



	protected function nonEmptyString( $message = '' )
	{
		if( is_string( $message ) and !empty( $message ) )
		{
			return true;
		}

		return false;
	}



	public function validType( $type = '' )
	{
		return in_array( $type, $this->types_allowed );
	}






	/////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////



	protected function getClass()
	{
		$class = ['notice-' . $this->type, $this->plugin->getSlug() ];

		if( $this->dismissable )
		{
			$class[]= 'is-dismissible';
		}

		return array_merge(	$this->default_class, $class );
	}






	/////////////////////////////////////////////
	// Rendering
	/////////////////////////////////////////////



	public function render()
	{
		$this->lines = $this->plugin->translate( $this->lines );
		
		$html = new Tag( 'div',	['class' => $this->getClass() ] );

		$p = new Tag( 'p' );

		foreach( $this->lines as $line )
		{
			$html->add( $p->setContent( $line, false ) );
		}

		$html->render();
	}







	/////////////////////////////////////////////
	// Static methods
	/////////////////////////////////////////////



	public static function error( WP_Plugin $plugin, $messages = '', $dismissable = true, $priority = 10 )
	{
		$notice = new WP_Admin_Notice( $plugin, [
			'lines' => $messages, 
			'type' => 'error',
			'dismissable' => $dismissable,
			'priority' => $priority,
		]);

		$notice->addAction();
	}



	public static function warning( WP_Plugin $plugin, $messages = '', $dismissable = true, $priority = 10 )
	{
		$notice = new WP_Admin_Notice( $plugin, [
			'lines' => $messages, 
			'type' => 'warning',
			'dismissable' => $dismissable,
			'priority' => $priority,
		]);

		$notice->addAction();
	}



	public static function success( WP_Plugin $plugin, $messages = '', $dismissable = true, $priority = 10 )
	{
		$notice = new WP_Admin_Notice( $plugin, [
			'lines' => $messages, 
			'type' => 'success',
			'dismissable' => $dismissable,
			'priority' => $priority,
		]);

		$notice->addAction();
	}



	public static function info( WP_Plugin $plugin, $messages = '', $dismissable = true, $priority = 10 )
	{
		$notice = new WP_Admin_Notice( $plugin, [
			'lines' => $messages, 
			'type' => 'info',
			'dismissable' => $dismissable,
			'priority' => $priority,
		]);

		$notice->addAction();
	}
}
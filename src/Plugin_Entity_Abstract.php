<?php
Namespace dgifford\WP_Plugin;



/*
	Provides methods for Plugin_Entity_Interface for injecting the plugin
	instance and setting properties.

	The constructor will automatically call these methods, then an init()
	method. This allows extending classes to use this constructor without
	having to define it and then do other specific operations in the
	init() method, or overwrite the constructor completely.


	Copyright (C) 2016  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



Use dgifford\Traits\PropertySetterTrait;
Use dgifford\Traits\MethodCallingTrait;



abstract class Plugin_Entity_Abstract implements Plugin_Entity_Interface
{
	Use PropertySetterTrait;
	Use MethodCallingTrait;



	// The plugin object
	protected $plugin;



	/**
	 * Default constructor injects plugin and sets properties
	 * @param Plugin $plugin
	 * @param Array $properties
	 */
	function __construct( Plugin $plugin, $properties = [] )
	{
		// Inject plugin instance
		$this->setPlugin( $plugin );

		// Set public properties
		$this->setProperties( $properties );

		// Check all properties
		$this->callAllSetters();

		// Call the init() method if it exists
		if( is_callable([$this, 'init']) )
		{
			$this->init();
		}
	}



	/**
	 * Injects the plugin object into the entity.
	 * @param plugin              $plugin
	 */
	public function setPlugin( Plugin $plugin )
	{
		$this->plugin = $plugin;
	}



	/**
	 * Set the entities properties
	 * @param string $properties  Array
	 */
	public function setProperties( $properties = '' )
	{
		$this->setPublicProperties( $properties );
	}
}
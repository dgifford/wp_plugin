<?php
Namespace dgifford\WP_Plugin;

/*
	Stores options in WP Database using a single array option.


    Copyright (C) 2016  Dan Gifford

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */




class Storage_Array extends Storage_DB
{
	// The name of the options array
	public $name = 'storage_array';

	// Holds the options array
	public $data = [];



	public function init()
	{
		$this->load();
	}



	/**
	 * Set the name of the options array
	 * @param string $name
	 */
	public function name( $name )
	{
		if( is_string( $name ) and !empty( $name ))
		{
			$this->name = $name;
		}
	}



	/**
	 * Loads the storage array
	 * @return null
	 */
	public function load()
	{
		$arr = parent::get( $this->name );

		if( is_array( $arr ) )
		{
			$this->data = $arr;
		}
	}



	/**
	 * Save the storage array
	 * @return null
	 */
	public function save()
	{
		parent::set( $this->name, $this->data );
	}



	/**
	 * Set key(s) with a value.
	 * 
	 * Accepts a key/ value pair or an array of pairs.
	 */
	public function set()
	{
		$args = func_get_args();

		if( count($args) == 1 and is_array( $args[0] ) )
		{
			$this->data = array_merge( $this->data, $args[0] );
		}
		elseif( count($args) == 1 and is_string( $args[0] ) )
		{
			$this->data[ $args[0] ] = null;
		}
		elseif( count($args) == 2 )
		{
			$this->data[ $args[0] ] = $args[1];
		}

		$this->save();
	}



	/**
	 * Get the value of a key. If it is null, return the default
	 * @param  string $key
	 * @return mixed 		The value
	 */
	public function get( $key )
	{
		if( isset( $this->data[ $key ] ) )
		{
			return $this->data[ $key ];
		}
		elseif( isset( $this->defaults[ $key ] ) )
		{
			return $this->defaults[ $key ];
		}

		return null;
	}



	/**
	 * Delete a key.
	 * @param  string $key
	 * @return bool 	 	Success
	 */
	public function delete( $key )
	{
		if( isset( $this->data[ $key ] ) )
		{
			unset( $this->data[ $key ] );

			$this->save();

			return true;
		}

		return false;
	}



	/**
	 * Deletes all keys.
	 * @return Null
	 */
	public function clear()
	{
		$this->data = [];
		
		$this->save();
		
		$this->clearDefaults();
	}



	/**
	 * Return an array of all keys without the plugin prefix.
	 *
	 * Combines data keys and default keys
	 * 
	 * @return array
	 */
	public function keys()
	{
		$combined = array_merge( $this->data, $this->defaults );

		return array_keys( $combined );
	}



	/**
	 * The uninstall script for the storage array.
	 * @return Null
	 */
	public function uninstall()
	{
		return "delete_option( '{$this->plugin->prefix( $this->name )}' );";
	}
}
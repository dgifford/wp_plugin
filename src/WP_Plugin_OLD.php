<?php
Namespace dgifford\WP_Plugin;



/*
	Class to hold Wordpress plugin information.


	Copyright (C) 2016  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



class WP_Plugin_Info
{
	protected $file = '';

	protected $name;

	protected $slug;

	protected $prefix;

	protected $path;

	protected $url;

	protected $text_domain;

	protected $domain_path;

	protected $dependencies = [];

	protected $reserved_terms =
	[
		'attachment',
		'attachment_id',
		'author',
		'author_name',
		'calendar',
		'cat',
		'category',
		'category__and',
		'category__in',
		'category__not_in',
		'category_name',
		'comments_per_page',
		'comments_popup',
		'custom',
		'customize_messenger_channel',
		'customized',
		'cpage',
		'day',
		'debug',
		'embed',
		'error',
		'exact',
		'feed',
		'hour',
		'link_category',
		'm',
		'minute',
		'monthnum',
		'more',
		'name',
		'nav_menu',
		'nonce',
		'nopaging',
		'offset',
		'order',
		'orderby',
		'p',
		'page',
		'page_id',
		'paged',
		'pagename',
		'pb',
		'perm',
		'post',
		'post__in',
		'post__not_in',
		'post_format',
		'post_mime_type',
		'post_status',
		'post_tag',
		'post_type',
		'posts',
		'posts_per_archive_page',
		'posts_per_page',
		'preview',
		'robots',
		's',
		'search',
		'second',
		'sentence',
		'showposts',
		'static',
		'subpost',
		'subpost_id',
		'tag',
		'tag__and',
		'tag__in',
		'tag__not_in',
		'tag_id',
		'tag_slug__and',
		'tag_slug__in',
		'taxonomy',
		'tb',
		'term',
		'terms',
		'theme',
		'title',
		'type',
		'w',
		'withcomments',
		'withoutcomments',
		'year',
	];










	public function __construct( $file = '', $dependencies = [] )
	{
		/*
			Ensure valid plugin file path is provided
		 */
		if( empty( $file ) or !file_exists( $file ) )
		{
			throw new \InvalidArgumentException('WP_Plugin requires a valid path to the plugin file.' );
		}



		/*
			Set dependencies
		 */
		$this->setDependencies( $dependencies );



		/*
			Get Plugin info
		 */
		$info = get_file_data($file, [ 'Name' => 'Plugin Name', 'TextDomain' => 'Text Domain', 'DomainPath' => 'Domain Path',] );
		


		/*
			Set plugin info
		 */
		$this->file 		= $file;
		$this->name 		= $info['Name'];		
		$this->slug 		= strtolower( pathinfo( $this->file, PATHINFO_FILENAME ) );
		$this->prefix 		= rtrim(str_replace(['-',' '], '_', $this->slug), '_' ) . '_';
		$this->path 		= dirname( $this->file );
		$this->url 			= plugin_dir_url( $this->file );
		$this->text_domain 	= $this->slug;
		$this->domain_path 	= '';

		if( !empty( $info['TextDomain'] ) )
		{
			$this->text_domain = $info['TextDomain'];
		}

		if( !empty( $info['DomainPath'] ) )
		{
			$this->domain_path = $info['DomainPath'];
		}



		/*
			Register hooks
		 */
		
		// DB setting allows notice to be shown after activation
		register_activation_hook( 	$this->file, [ $this, 'onActivation'] );
		register_deactivation_hook( $this->file, [ $this, 'onDeactivation'] );



		/*
			Check dependencies
		 */
		if( $this->getFlag( 'check_dependencies' ) )
		{
			add_action( 'plugins_loaded', [ $this, 'checkDependencies' ]);
		}
	}







	/////////////////////////////////////////////
	// Getters
	/////////////////////////////////////////////



	public function getProperties()
	{
		return get_object_vars( $this );
	}



	public function getTextDomain()
	{
		if( !empty( $this->text_domain ) )
		{
			return $this->text_domain;
		}
		else
		{
			return $this->getSlug();
		}
	}



	public function getFile()
	{
		return $this->file;
	}



	public function getDirectory( $path = '' )
	{
		if( empty( $path ) )
		{
			return $this->path;
		}
		else
		{
			return rtrim( $this->path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . ltrim( $path, DIRECTORY_SEPARATOR );
		}
	}



	/**
	 * Returns URL to plugin directory with $path added on if provided.
	 * 
	 * @param  string $path  	Path within plugin directory
	 * @return string         	URL
	 */
	public function getURL( $path = '' )
	{
		if( empty( $path ) )
		{
			return $this->url;
		}
		else
		{
			return rtrim( $this->url, '/') . '/' . ltrim( $path, '/' );
		}
	}



	public function getName()
	{
		return $this->name;
	}



	public function getSlug()
	{
		return $this->slug;
	}



	/**
	 * Returns plugin prefix with $suffix added on if provided.
	 * 
	 * @param  string $suffix  	String to concatenate
	 * @return string
	 */
	public function getPrefix( $suffix = '' )
	{
		if( empty( $suffix ) )
		{
			return $this->prefix;
		}
		else
		{
			return $this->prefix . ltrim( $suffix, '_' );
		}
	}



	public function isReservedTerm( $term = '' )
	{
		return in_array( $term, $this->reserved_terms );
	}



	/**
	 * Return an array of all plugin paths indexed by plugin names.
	 * 
	 * @return array
	 */
	public function getPluginPaths()
	{
		/*
			Get list of plugins.
			https://codex.wordpress.org/Function_Reference/get_plugins
		 */ 
		if ( ! function_exists( 'get_plugins' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		$result = [];
		$plugins = get_plugins();

		foreach( $plugins as $path => $plugin )
		{
			$result[ $plugin['Name'] ] = $path;
		}

		return $result;
	}






	/////////////////////////////////////////////
	// WP plugin activation/ deactivation
	/////////////////////////////////////////////



	public function setDependencies( $dependencies = [] )
	{
		if( !empty( $dependencies ) )
		{
			if( is_string( $dependencies ) )
			{
				$this->dependencies = [ $dependencies ];
			}
			elseif( is_array( $dependencies ) )
			{
				$this->dependencies = $dependencies;
			}
		}
	}



	/**
	 * Checks to see if dependencies are installed by checking if the strings match:
	 * 		A plugin name (case sensitive)
	 * 		A plugin path
	 * 		A function name
	 * 		A class name
	 *
	 * If a plugin is found, whether it is activated is also checked.
	 * 
	 * @return [type] [description]
	 */
	public function checkDependencies()
	{
		if( empty( $this->dependencies ) )
		{
			return;
		}

		$plugins = $this->getPluginPaths();

		$messages = [];

		foreach( $this->dependencies as $dependency )
		{
			if
			(
				!isset( $plugins[ $dependency ] )
				and !in_array( $dependency, $plugins )
				and !function_exists( $dependency )
				and !class_exists( $dependency )
			)
			{
				$messages[] = $this->getName() . " is dependant on '{$dependency}' which cannot be found.";
			}

			if(
				(isset( $plugins[ $dependency ] ) and !is_plugin_active( $plugins[ $dependency ] ))
				or
				(in_array( $dependency, $plugins ) and !is_plugin_active( $dependency ))
			)
			{
				$messages[] = $this->getName() . " is dependant on '{$dependency}' which is not activated.";
			}


			if( !empty( $messages ) )
			{
				deactivate_plugins( plugin_basename( $this->file ) );

				unset($_GET['activate']);

				WP_Admin_Notice::error( $this, $messages );

				$this->deleteFlag( 'activated' );
			}
		}
	}



	public function checkPermission( $value = '' )
	{
		if( !current_user_can( 'install_plugins' ) )
		{
			wp_die( __( 'You do not have sufficient permissions to use this plugin.' ), 403 );
		}
	}



	/**
	 * Method called on activation.
	 * Must use database settings (or session/ cookie?) to persist state.
	 * 
	 * @return null
	 */
	public function onActivation()
	{
		$this->deleteFlag( 'deactivated' );

		$this->setFlag( 'activated' );

		if( $this->hasDependencies() )
		{
			$this->setFlag( 'check_dependencies' );
		}
	}



	public function onDeactivation()
	{
		$this->checkPermission();

		$this->setFlag( 'deactivated' );
	}



	public function hasDependencies()
	{
		if( !empty( $this->dependencies ) )
		{
			return true;
		}

		return false;
	}



	/**
	 * Translate an array of strings.
	 * @param  array  $arr
	 * @return array
	 */
	public function translate( $arr = [], $text_domain = '' )
	{
		if( empty($text_domain) )
		{
			$text_domain = $this->getTextDomain();
		}

		if( is_string( $arr ) )
		{
			$arr = [ $arr ];
		}

		foreach( $arr as $key => $value )
		{
			if( is_string($value) and !empty($value) )
			{
				$arr[$key] = __( $value, $text_domain );
			}
		}

		return $arr;
	}






	/////////////////////////////////////////////
	// Methods for persisting info (flags) in DB
	/////////////////////////////////////////////











	/////////////////////////////////////////////
	// Uninstall
	/////////////////////////////////////////////



	public function setUninstallSettings( $settings = [] )
	{
		if( !empty( $settings ) )
		{
			$this->uninstall['settings'] = $settings;
		}
	}



	public function setUninstallSettingsGroup( $settings_group = '' )
	{
		if( !empty( $settings_group ) )
		{
			$this->uninstall['settings_group'] = $settings_group;
		}
	}



	public function getUninstallScript()
	{
		$code = '';

		if( isset($this->uninstall['settings']) and isset($this->uninstall['settings_group']) )
		{
			$code .= '$settings_group = "' . $this->uninstall['settings_group'] . '";$option_names = ["' . implode('", "', $this->uninstall['settings'] ) . '"];' . "\n\n";

			$code .= 'foreach( $option_names as $name ){ unregister_setting( $settings_group, $name );}' . "\n\n";

		}

		return $code;
	}



	public function saveUninstaller()
	{
		$code = $this->getUninstallScript();

		if( !empty( $code ) )
		{
			if( !is_writable( $this->getDirectory() ) )
			{
				add_action('admin_notices', [ $this, 'cannotCreateUninstallerNotice' ] );
			}

			$code = "<?php\nif( !defined( 'WP_UNINSTALL_PLUGIN' ) or !current_user_can( 'activate_plugins' ) ) { 	wp_die( __( 'You do not have sufficient permissions to access this page.' ), 403 ); }\n\n" . $code;

			if( $result = file_put_contents( $this->getDirectory() . '/uninstall.php', $code) === false )
			{
				add_action('admin_notices', [ $this, 'cannotWriteUninstallerError' ] );
			}
		}
	}



	public function cannotWriteUninstallerError()
	{
		echo "<div class='error'><p>{$this->getName()} cannot write uninstall.php to {$this->getDirectory()}.</p></div>";
	}



	public function cannotCreateUninstallerNotice()
	{
		echo "<div class='updated'><p>{$this->getName()} cannot create the uninstaller.</p></div>";
	}

}
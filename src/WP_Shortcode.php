<?php
Namespace dgifford\WP_Plugin;



/*
	Class for creating a Wordpress shortcode.
 

    Copyright (C) 2016  Dan Gifford

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */




abstract class WP_Shortcode extends WP_Entity_Abstract
{
	// Tag name of the shortcode
	public $tag = '';

	// Attributes passed to shortcode
	protected $attributes = [];

	// Default attributes
	protected $defaults = [];

	// Filters for attributes
	protected $attribute_filters = [];

	// Content between shortcode tags
	protected $content = '';

	// Filters for the content
	protected $content_filters = '';






	public function register()
	{
		add_shortcode( $this->getTag(), [ $this, 'parse' ] );
	}




	public function getTag()
	{
		if( is_string( $this->tag ) and !empty( $this->tag ) )
		{
			return $this->tag;
		}
		
		return substr( __CLASS__, strlen(__NAMESPACE__));
	}




	public function parse( $attributes = [], $content = '', $tag )
	{
		$this->attributes = shortcode_atts( 
			$this->defaults, 
			$attributes, 
			$tag
		);

		$this->content = $content;

		$this->filterAttributes();

		$this->filterContent();

		$this->action();		
	}



	public function getDefaultAttributes( $tag = '' )
	{
		if( isset( $this->shortcodes[ $tag ]['defaults'] ) )
		{
			return $this->shortcodes[ $tag ]['defaults'];
		}

		return [];
	}



	protected function filterAttributes( $tag = '' )
	{
		if( isset( $this->shortcodes[ $tag ]['required'] ) )
		{
			return $this->shortcodes[ $tag ]['required'];
		}

		return [];
	}

}

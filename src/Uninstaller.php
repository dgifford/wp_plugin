<?php
Namespace dgifford\WP_Plugin;



/*
	Class to generate Wordpress plugin uninstall.php script.


	Copyright (C) 2017  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



class Uninstaller extends Plugin_Entity_Abstract
{
	// The uninstaller script
	public $script = "<?php
/*
	Wordpress Plugin auto-generated uninstall script.


	Copyright (C) 2017  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



if( !defined( 'WP_UNINSTALL_PLUGIN' ) or !current_user_can( 'activate_plugins' ) )
{
	wp_die( __( 'You do not have sufficient permissions to access this page.' ), 403 );
}

";

	// The uninstaller filename
	public $filename = 'uninstall.php';




	/**
	 * Initialise by saving the uninstall script if required
	 * 
	 * @return null
	 */
	public function init()
	{
		if( !$this->exists() and !$this->plugin->inRoot() )
		{
			$this->save();
		}
	}



	/**
	 * Returns true if the uninstall file already exists
	 * @return bool
	 */
	public function exists()
	{
		if( file_exists( $this->plugin->path( $this->filename ) ))
		{
			return true;
		}

		return false;
	}



	/**
	 * Get the script for the uninstaller
	 * @return null
	 */
	public function script()
	{
		// Add any uninstall for options.
		$this->script .=  $this->plugin->options->uninstall();

		return $this->script;
	}



	/**
	 * Save the uninstall script. Display errors via WP admin notices
	 * if the save fails.
	 * @return null
	 */
	protected function save()
	{
		$script = $this->script();

		if( !empty( $script) )
		{
			if( !is_writable( $this->plugin->path() ) )
			{
				Notice::error( $this->plugin->slug(), '{$this->plugin->name()} cannot create the uninstaller because {$this->plugin->path()} is not writeable.' );
			}

			if( $result = file_put_contents( $this->plugin->path( $this->filename ), $script ) === false )
			{
				Notice::error( $this->plugin->slug(), '{$this->plugin->name()} encountered an error writing to {$this->plugin->path()}' );
			}
		}
	}
}
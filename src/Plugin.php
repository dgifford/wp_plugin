<?php
Namespace dgifford\WP_Plugin;



/*
	Wordpress plugin base class. By default, it:

	- Holds plugin information
	- creates an options object to store Information for the plugin
	  in the WP database
	- Creates the uninstall script


	Copyright (C) 2016  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



Use dgifford\html\Prefab;



class Plugin
{
	// Plugin info
	protected $file = '';
	protected $name;
	protected $slug;
	protected $prefix;
	protected $path;
	protected $url;
	protected $text_domain;
	protected $domain_path;

	// Plugin options object
	public $options;

	// Uninstaller
	public $uninstaller;

	// Default message shown on activation
	protected $activation_message;






	/**
	 * Create the plugin.
	 * 
	 * @param string $file   The path to the plugin file
	 */
	public function __construct( $file )
	{
		if( empty( $file ) or !file_exists( $file ) )
		{
			throw new \InvalidArgumentException('A valid path to the plugin file is required.' );
		}

		// Generate info from file path
		$info = get_file_data($file, [ 'Name' => 'Plugin Name', 'TextDomain' => 'Text Domain', 'DomainPath' => 'Domain Path',] );

		/*
			Set plugin info
		 */
		$this->file 		= $file;
		$this->name 		= $info['Name'];		
		$this->slug 		= strtolower( pathinfo( $this->file, PATHINFO_FILENAME ) );
		$this->prefix 		= rtrim(str_replace(['-',' '], '_', $this->slug), '_' ) . '_';
		$this->path 		= dirname( $this->file );
		$this->url 			= plugin_dir_url( $this->file );
		$this->text_domain 	= $this->slug;
		$this->domain_path 	= '';

		if( !empty( $info['TextDomain'] ) )
		{
			$this->text_domain = $info['TextDomain'];
		}

		if( !empty( $info['DomainPath'] ) )
		{
			$this->domain_path = $info['DomainPath'];
		}

		// Create options
		$this->options = new Storage_Array( $this, ['name' => 'info'] );

		// Set path for html prefab
		Prefab::addPath( __DIR__ . '/prefab' );

		// Register hooks
		register_activation_hook( 	$this->file(), [ $this, 'onActivation'] );
		register_deactivation_hook( $this->file(), [ $this, 'onDeactivation'] );

		// Notices on plugin activation
		add_action( 'plugins_loaded', [ $this, 'showActivationNotice' ]);
	}



	/**
	 * Adds a plugin entity (settings page, shortcodes etc)
	 * @param [type] $entity_name [description]
	 * @param [type] $properties  [description]
	public function add( $entity_name, $properties )
	{
		if( $entity_name instanceof Plugin_Entity_Interface )
		{
			$this->
		}
	}
	 */
	


	/**
	 * Method called on activation.
	 * Uses options class to persist state.
	 * 
	 * @return null
	 */
	public function onActivation()
	{
		$this->options->set( 'plugin_state', 'activated' );

		$this->options->set( 'show_activation_notice', 1 );

		$this->uninstaller = new Uninstaller( $this );
	}



	/**
	 * Shows activation notice.
	 * 
	 * @return null
	 */
	public function showActivationNotice()
	{
		if( $this->options->get( 'show_activation_notice' ) and !empty( $this->getActivationMessage()) )
		{
			Notice::success( $this->slug(), $this->getActivationMessage() );

			$this->options->delete( 'show_activation_notice' );
		}
	}



	/**
	 * Method hooked into deactivation
	 * 
	 * @return null
	 */
	public function onDeactivation()
	{
		$this->checkPermission();

		$this->options->set( 'plugin_state', 'deactivated' );
	}



	public function checkPermission()
	{
		if( !current_user_can( 'install_plugins' ) )
		{
			wp_die( __( 'You do not have sufficient permissions to use this plugin.' ), 403 );
		}
	}



	/**
	 * Translate an array of strings using the plugin's text domain
	 * @param  array  $arr
	 * @return array
	 */
	public function translate( $arr = [], $text_domain = '' )
	{
		if( empty($text_domain) )
		{
			$text_domain = $this->textDomain();
		}

		if( is_string( $arr ) )
		{
			$arr = [ $arr ];
		}

		foreach( $arr as &$value )
		{
			if( is_string($value) and !empty($value) )
			{
				$value = __( $value, $text_domain );
			}
		}

		return $arr;
	}



	/**
	 * Returns true if the main plugin file is in the root
	 * of the WP plugins folder
	 * @return bool
	 */
	public function inRoot()
	{
		if( ABSPATH . 'wp-content/plugins' == $this->path() )
		{
			return true;
		}

		return false;
	}






	/////////////////////////////////////////////
	// Getters for plugin properties
	/////////////////////////////////////////////



	
	/**
	 * Sets the activation message property.
	 * 
	 * @return string
	 */
	public function setActivationMessage( $activation_message )
	{
		if( is_string($activation_message) )
		{
			$this->activation_message = $activation_message;
		}
	}



	/**
	 * Returns the activation message property.
	 * 
	 * @return string
	 */
	public function getActivationMessage()
	{
		if( is_string($this->activation_message) )
		{
			return $this->activation_message;
		}

		return '';
	}



	/**
	 * Return an array of the plugins properties
	 * @return array
	 */
	public function properties()
	{
		return get_object_vars( $this );
	}



	/**
	 * Return the plugin text domain. Defaults to the slug.
	 * @return string
	 */
	public function textDomain()
	{
		if( !empty( $this->text_domain ) )
		{
			return $this->text_domain;
		}
		else
		{
			return $this->getSlug();
		}
	}



	/**
	 * Return the plugin file path.
	 * @return string
	 */
	public function file()
	{
		return $this->file;
	}



	/**
	 * Returns the plugin folder path.
	 * Accepts a path string to concatenate with the plugin path.
	 * 
	 * @param  string $path
	 * @return string
	 */
	public function path( $path = '' )
	{
		if( empty( $path ) )
		{
			return $this->path;
		}
		else
		{
			return rtrim( $this->path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . ltrim( $path, DIRECTORY_SEPARATOR );
		}
	}



	/**
	 * Returns URL to plugin directory with $path added on if provided.
	 * 
	 * @param  string $path  	Path within plugin directory
	 * @return string         	URL
	 */
	public function url( $path = '' )
	{
		if( empty( $path ) )
		{
			return $this->url;
		}
		else
		{
			return rtrim( $this->url, '/') . '/' . ltrim( $path, '/' );
		}
	}


	/**
	 * Returns the plugin name.
	 * 
	 * @return string
	 */
	public function name()
	{
		return $this->name;
	}



	/**
	 * Returns the plugin slug.
	 * 
	 * @return string
	 */
	public function slug( $suffix = '' )
	{
		if( empty( $suffix ) )
		{
			return $this->slug;
		}
		else
		{
			return $this->slug . '-' . ltrim( $suffix, '-' );
		}
	}



	/**
	 * Returns plugin prefix with $suffix added on if provided.
	 * 
	 * @param  string $suffix  	String to concatenate
	 * @return string
	 */
	public function prefix( $suffix = '' )
	{
		if( empty( $suffix ) )
		{
			return $this->prefix;
		}
		else
		{
			return $this->prefix . ltrim( $suffix, '_' );
		}
	}

}
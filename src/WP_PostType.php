<?php
Namespace dgifford\WP_Plugin;



/**
 * Class to create Wordpress custom post types
 * 
 */



class WP_PostType extends WP_Entity_Abstract
{
	protected $validator_filters = 'maxlength|20, minlength|1';

	protected $sanitizer_filters = 'lowercase, name_attribute';






	/**
	 * Add any WP hooks required by entity
	 * @return null
	 */
	public function hooks()
	{
		add_action( 'init', [ $this, 'registerPostTypes'] );
	}



	/**
	 * Register the post types
	 * @return null
	 */
	public function registerPostTypes()
	{
		foreach( $this->container as $post_type => $arguments )
		{
			register_post_type( $post_type, $arguments );
		}
	}



	/**
	 * Use the post type name if no labels defined.
	 * 
	 * @param  string $name
	 * @param  array $properties
	 * @return array
	 */
	protected function validateProperties( $name, $properties )
	{
		if( !isset( $properties['label'] ) and !isset( $properties['labels']['name'] ) )
		{
			$properties['label'] = $name;
		}

		if( isset( $properties['label'] ) )
		{
			$properties['label'] = $this->plugin->translate( $properties['label'] );
		}

		if( isset( $properties['labels'] ) )
		{
			$properties['labels'] = $this->plugin->translate( $properties['labels'] );
		}

		return $properties;
	}
}
<?php
Namespace dgifford\WP_Plugin;



/**
 * Class to create Wordpress custom taxonomies
 * 
 */



class WP_Taxonomy extends WP_Entity_Abstract
{
	protected $validator_filters = 'maxlength|32, minlength|1';

	protected $sanitizer_filters = 'lowercase, name_attribute';






	/**
	 * Add any WP hooks required by entity
	 * @return null
	 */
	public function hooks()
	{
		add_action( 'init', [ $this, 'registerTaxonomies'] );
	}



	/**
	 * Register the taxonomies
	 * @return null
	 */
	public function registerTaxonomies()
	{
		foreach( $this->container as $taxonomy => $p )
		{
			register_taxonomy( $taxonomy, $p['post_type'], $p['arguments'] );

			if( !empty($p['post_type']) )
			{
				register_taxonomy_for_object_type( $taxonomy, $p['post_type'] );
			}
		}
	}



	/**
	 * Validate the taxonomy properties.
	 * 
	 * @param  string $name
	 * @param  array $properties
	 * @return array
	 */
	protected function validateProperties( $name, $p )
	{
		if( !isset( $p['post_type'] ) )
		{
			$p['post_type'] = '';
		}
		
		if( !isset( $p['arguments'] ) )
		{
			$p['arguments'] = [];
		}

		if( !isset( $p['arguments']['label'] ) and !isset( $p['arguments']['labels']['name'] ) )
		{
			$p['arguments']['label'] = $name;
		}

		if( isset( $properties['arguments']['label'] ) )
		{
			$p['arguments']['label'] = $this->plugin->translate( $p['arguments']['label'] );
		}

		if( isset( $properties['arguments']['labels'] ) )
		{
			$p['arguments']['labels'] = $this->plugin->translate( $p['arguments']['labels'] );
		}

		return $p;
	}
}
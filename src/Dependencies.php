<?php
Namespace dgifford\WP_Plugin;



/*
	Manages plugin dependencies.


	Copyright (C) 2016  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



class Dependencies extends Plugin_Entity_Abstract
{
	/**
	 * Set the dependencies
	 * @param string $properties
	 */
	public function set( $properties = '' )
	{
		if( is_string( $properties ) )
		{
			$this->properties = [ $properties ];
		}
		elseif( is_array( $properties ) )
		{
			$this->properties = $properties;
		}
	}



	public function exist()
	{
		return !empty( $this->properties );
	}



	/**
	 * Checks to see if dependencies are installed by checking if the strings match:
	 * 		A plugin name (case sensitive)
	 * 		A plugin path
	 * 		A function name
	 * 		A class name
	 *
	 * If a plugin is found, whether it is activated is also checked.
	 * 
	 * @return [type] [description]
	 */
	public function check()
	{
		if( empty( $this->dependencies ) )
		{
			return;
		}

		$plugins = $this->getPluginPaths();

		$messages = [];

		foreach( $this->dependencies as $dependency )
		{
			if
			(
				!isset( $plugins[ $dependency ] )
				and !in_array( $dependency, $plugins )
				and !function_exists( $dependency )
				and !class_exists( $dependency )
			)
			{
				$messages[] = $this->info->name() . " is dependant on '{$dependency}' which cannot be found.";
			}

			if(
				(isset( $plugins[ $dependency ] ) and !is_plugin_active( $plugins[ $dependency ] ))
				or
				(in_array( $dependency, $plugins ) and !is_plugin_active( $dependency ))
			)
			{
				$messages[] = $this->info->name() . " is dependant on plugin '{$dependency}' which is not activated.";
			}


			if( !empty( $messages ) )
			{
				deactivate_plugins( plugin_basename( $this->file ) );

				unset($_GET['activate']);

				WP_Notice::error( $this, $messages );

				$this->options->delete( 'activated' );
			}
		}
	}



	/**
	 * Return an array of all plugin paths indexed by plugin names.
	 * 
	 * @return array
	 */
	public function getPluginPaths()
	{
		/*
			Get list of plugins.
			https://codex.wordpress.org/Function_Reference/get_plugins
		 */ 
		if( ! function_exists( 'get_plugins' ) )
		{
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		$result = [];
		$plugins = get_plugins();

		foreach( $plugins as $path => $plugin )
		{
			$result[ $plugin['Name'] ] = $path;
		}

		return $result;
	}
}
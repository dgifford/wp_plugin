<?php
Namespace dgifford\WP_Plugin;



/*
	Class for creating a Wordpress shortcode.
 

    Copyright (C) 2016  Dan Gifford

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



Use dgifford\Filter\Validator;



abstract class Shortcode extends Plugin_Entity_Abstract
{
	// Tag name of the shortcode
	public $tag = '';

	// Attributes that can be included, with the default values
	public $defaults = [];

	// Validator object used to validate attributes
	public $validator;

	// Array of filters used by validator indexed by attribute name
	public $validation_filters = [];

	// Attributes passed to shortcode
	protected $attributes = [];

	// Content between shortcode tags
	protected $content = '';

	// Filters for attributes
	protected $attribute_filters = [];

	// Filters for the content
	protected $content_filters = '';





	/**
	 * Initialise the shortcode, called at end of constructor.
	 * 
	 * @return null
	 */
	public function init()
	{
		// TODO: test if shortcode exists already?

		// Register the shortcode
		add_shortcode( $this->tag, [ $this, 'parse' ] );
	}



	/**
	 * Parse the content and attributes sent to the Shortcode, 
	 * validate them, then call the action method.
	 * 
	 * @param  array  $attributes
	 * @param  string $content 
	 * @param  string $tag
	 * @return null
	 */
	public function parse( $attributes = [], $content = '', $tag )
	{
		$this->attributes = shortcode_atts( 
			$this->defaults, 
			$attributes, 
			$tag
		);

		$this->validateAttributes();

		$this->setContent( $content );

		return $this->action();
	}



	/**
	 * Check each attribute against its validation filters.
	 * If it fails, replace it with the default.
	 * 
	 * @return null
	 */
	public function validateAttributes()
	{
		foreach( $this->validation_filters as $name => $filter )
		{
			$this->validator->set( $this->attributes[ $name ], $filter );

			if( !$this->validator->isValid() )
			{
				$this->attributes[ $name ] = $this->defaults[ $name ];
			}
		}
	}



	/**
	 * Set the shortcode content.
	 * 
	 * @param string $content
	 */
	public function setContent( $content = '' )
	{
		$this->content = $content;
	}



	/**
	 * The method that performs the shortcode actions.
	 * 
	 * @return mixed
	 */
	public function action()
	{
	}



	/**
	 * Set the tag name. If none is supplied, use the classname.
	 * 
	 * @return null
	 */
	public function set_tag( $tag = '' )
	{
		if( !is_string( $tag ) or empty( $tag ) )
		{
			$tag = substr( get_class($this), strlen(__NAMESPACE__) + 1);
		}

		$this->tag = $tag;
	}



	/**
	 * Set the validation_filters array
	 * @param array $validation_filters
	 */
	public function set_validation_filters( $validation_filters = [] )
	{
		if( is_array( $validation_filters ) )
		{
			$this->validation_filters = $validation_filters;
		}
	}



	/**
	 * Sets the Validator object
	 * @param Validator $validator
	 */
	public function set_validator( $validator = null )
	{
		if( !($validator instanceof Validator) )
		{
			$validator = New Validator;
		}

		$this->validator = $validator;
	}
}

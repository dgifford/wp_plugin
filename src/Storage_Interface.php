<?php
Namespace dgifford\WP_Plugin;

/*
	Interface for storage of persistent data


    Copyright (C) 2015  Dan Gifford

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */




interface Storage_Interface
{
	/**
	 * Set default(s)
	 * 
	 * Accepts a key/ value pair or an array of defaults.
	 */
	public function default();



	/**
	 * Clear defaults
	 * 
	 */
	public function clearDefaults();



	/**
	 * Boolean test for if a default is set.
	 * 
	 * @param  string  $key
	 * @return boolean
	 */
	public function hasDefault( $key );



	/**
	 * Set a key/ value pair.
	 * 
	 * Accepts a key/ value pair or an array of values.
	 * If an array is provided, they are appended to the
	 * existing key/ value pairs.
	 */
	public function set();



	/**
	 * Get the value of a key. If it is null, return the default
	 * @param  string $key
	 * @return mixed 		The value
	 */
	public function get( $key );



	/**
	 * Return an array of all keys without the plugin prefix.
	 *
	 * Combines data keys and default keys
	 * 
	 * @return array
	 */
	public function keys();



	/**
	 * Get an array of all key/ value pairs
	 * 
	 * @return array
	 */
	public function getAll();



	/**
	 * Delete a key/ value pair. Returns true if deleted successfully, else false.
	 * @param  string $key
	 * @return bool
	 */
	public function delete( $key );



	/**
	 * Returns a count of all keys set.
	 *
	 * Does not include defaults.
	 * 
	 * @return int
	 */
	public function count();



	/**
	 * Deletes all keys.
	 * @return Null
	 */
	public function clear();



	/**
	 * Returns true if an key exists, else false.
	 * @param  string $key
	 * @return bool
	 */
	public function exists( $key );



	/**
	 * Return the uninstall script for these keys.
	 * @return Null
	 */
	public function uninstall();
}
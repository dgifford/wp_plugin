<?php
Namespace dgifford\WP_Plugin;



/*
	Class to generate Wordpress plugin uninstall script.


	Copyright (C) 2017  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



class WP_Uninstall extends WP_Entity_Abstract
{
	public $code = '';

	public $filename = 'uninstall.php';






	/**
	 * Initialise by saving the uninstall script if it doesn't
	 * already exist.
	 * 
	 * @return null
	 */
	public function init()
	{
		if( !file_exists( $this->plugin->getDirectory( $this->filename ) ))
		{
			// Add any script to uninstall options.
			$options_uninstall =  this->options->uninstall();

			if( !empty( $options_uninstall ) )
			{
				$this->code .= $options_uninstall;
			}

			$this->saveScript();
		}
	}




	/**
	 * Set the code used for the uninstaller.
	 * 
	 * @param string $code
	 */
	public function set_code( $code = '' )
	{
		if( is_string( $code ) and !empty( $code ) )
		{
			$this->code = $code;
		}
		elseif( $default = file_get_contents( __DIR__ . '/uninstall_default.php' ) )
		{
			$this->code = $default;
		}
		else
		{
			throw new \InvalidArguementException( 'No default uninstall code.' );			
		}
	}



	/**
	 * Add the code to uninstall the options.
	 * @return null
	 */
	public function getScript()
	{
		return $this->code;
	}



	/**
	 * Save the uninstall script. Display errors via WP admin notices
	 * if the save fails.
	 * @return null
	 */
	public function saveScript()
	{
		if( !is_writable( $this->plugin->getDirectory() ) )
		{
			WP_Admin_Notice::error( $this->plugin, '{$this->plugin->getName()} cannot create the uninstaller because {$this->plugin->getDirectory()} is not writeable.' );
		}

		if( $result = file_put_contents( $this->plugin->getDirectory( $this->filename ), $this->getScript() ) === false )
		{
			WP_Admin_Notice::error( $this->plugin, '{$this->plugin->getName()} encountered an error writing to {$this->plugin->getDirectory()}' );
		}
	}
}
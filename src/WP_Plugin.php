<?php
Namespace dgifford\WP_Plugin;



/*
	Container for Wordpress plugins


	Copyright (C) 2016  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



Use dgifford\html\Prefab;



class WP_Plugin
{
	// Info object
	public $info;

	// Plugin options object
	public $state;

	// Uninstaller
	public $uninstaller;






	/**
	 * Create the plugin container.
	 * Accepts an array of properties or the path to the plugin file as a string.
	 *
	 * Creates an Options object by default
	 * 
	 * @param array $properties
	 */
	public function __construct( $properties = [] )
	{
		if( is_array($properties) )
		{
			extract( $properties );
		}
		elseif( is_string( $properties ) )
		{
			$file = $properties;
		}

		if( realpath($file) === false )
		{
			throw new \InvalidArguementException("A valid path to the plugin file is required");
		}
		else
		{
			$this->setInfo( $file );
		}

		// Create option to store plugin state
		$this->state = new Storage_DB( $this->info );

		// Set path for html prefab
		Prefab::addPath( __DIR__ . '/prefab' );

		// Register hooks
		register_activation_hook( 	$this->info->file(), [ $this, 'onActivation'] );
		register_deactivation_hook( $this->info->file(), [ $this, 'onDeactivation'] );
	}



	/**
	 * Creates the plugin info object from the path to the plugin file
	 * @param string $file
	 */
	public function setInfo( $file )
	{
		$this->info = new Info( $file );
	}



	/**
	 * Creates the plugin options object.
	 * Accepts an object implementing the Options_Interface or
	 * 
	 * 
	 * @param mixed $options
	 */
	public function setOptions( $options = null )
	{
		if( !($options instanceof Options_Interface) )
		{
			$options = new Options_Array( $this->info );
		}

		$this->options = $options;
	}



	/**
	 * Sets plugin default options.
	 * @param string/array  	defaults 	Array or string path to ini file
	 */
	public function setDefaults( $defaults )
	{
		// Create default options object if none created.
		if( !$this->hasOptions() )
		{
			$this->setOptions();
		}

		// Path to ini file provided
		if( is_string( $defaults ) and realpath($defaults) !== false )
		{
			$defaults = parse_ini_file( $defaults );
		}

		if( is_array( $defaults ) )
		{
			$this->options->setDefaults( $defaults );
			return;
		}

		throw new \InvalidArguementException("Default option values must be an array or a path to an existing ini file.");
	}



	/**
	 * Creates the plugin dependencies object
	 * @param string $file
	 */
	public function setDependencies( $dependencies )
	{
		// Create default options object if none created.
		if( !$this->hasOptions() )
		{
			$this->setOptions();
		}

		// Set dependencies object or create from an array/ string
		if( !($dependencies instanceof Dependencies) )
		{
			$dependencies = new Dependencies( $this->info, $this->options, $dependencies );
		}

		$this->dependencies = $dependencies;
	}



	/**
	 * Creates the plugin Settings Page.
	 * Accepts a Settings_Page object or an array of field definitions.
	 * @param string $file
	 */
	public function setSettingsPage( $settings_page )
	{
		// Set dependencies object or create from an array/ string
		if( !($settings_page instanceof Settings_Page) )
		{
			$settings_page = new Settings_Page( $this->info, $this->options, $settings_page );
		}

		$this->settings_page = $settings_page;
	}



	/**
	 * Returns true if the plugin has a valid options object
	 * @return boolean [description]
	 */
	public function hasOptions()
	{
		if( $this->options instanceof Options_Interface )
		{
			return true;
		}

		return false;
	}



	/**
	 * Method called on activation.
	 * Uses options class to persist state.
	 * 
	 * @return null
	 */
	public function onActivation()
	{
		$this->state->set( 'plugin_state', 'activated' );

		$this->uninstaller = new Uninstaller( $this->info, $this->options );
	}



	public function onDeactivation()
	{
		$this->checkPermission();

		$this->state->set( 'plugin_state', 'deactivated' );
	}



	public function checkPermission()
	{
		if( !current_user_can( 'install_plugins' ) )
		{
			wp_die( __( 'You do not have sufficient permissions to use this plugin.' ), 403 );
		}
	}

}
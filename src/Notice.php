<?php
Namespace dgifford\WP_Plugin;



/*
	Class to generate Wordpress admin notices.


	Copyright (C) 2017  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



Use dgifford\Tag\Tag;
Use dgifford\html\Prefab;



Use dgifford\Traits\PropertySetterTrait;
Use dgifford\Traits\ArrayHelpersTrait;



class Notice
{
	Use PropertySetterTrait;
	Use ArrayHelpersTrait;



	// Paragraphs displayed in message
	public $lines = [];
	
	// Whether the Notice should be dismissable
	public $dismissable = true;

	// Notice type
	public $type = 'info';

	// Notice priority
	public $priority = 10;

	// WP admin pages on which the message should display
	// Leave blank for all
	public $pages = [];
	
	// Current WP notice types
	protected $types_allowed = [ 'error', 'warning', 'success', 'info', ];

	// Class names added to Notice
	protected $class = [];







	public function __construct()
	{
		if( func_num_args() > 0 )
		{
			return call_user_func_array([ $this, 'set'], func_get_args() );
		}
	}



	public function setHTML( $html = null )
	{
		if( $html instanceof Prefab )
		{
			$this->html = $html;
		}
		else
		{
			$this->html = Prefab::notice();
		}
	}



	/**
	 * Accepts an array of arguments indexed by property name,
	 * a single string message or a class followed by a message
	 * or array of messages.
	 *
	 * Any arguments can be a boolean for setting dismissable or
	 * an int for setting priority.
	 */
	public function set()
	{
		$args = func_get_args();

		// Set dismissable property and priority property
		foreach( $args as $key => $arg )
		{
			if( is_bool($arg) )
			{
				$this->dismissable = $arg;
				unset($args[$key]);
			}

			if( is_int($arg) )
			{
				$this->priority = $arg;
				unset($args[$key]);
			}
		}

		switch( count( $args ) )
		{
			case 1:
				// Array of properties
				if( !$this->isNumericallyIndexed( $args[0] ) and is_array( $args[0] ) )
				{
					$this->setPublicProperties( $args[0] );
				}
				// Single string message or array of strings
				elseif( is_array( $args[0] ) or is_string( $args[0] ) )
				{
					$this->set_lines( $args[0] );
				}
			break;


			
			case 2:
				if( is_string( $args[0] ) and (is_string( $args[1] ) or is_array( $args[1] )) )
				{
					$this->class[] = $args[0];
					$this->set_lines( $args[1] );
				}
				elseif( is_array( $args[0] ) and is_string( $args[1] ) )
				{
					$this->set_lines( $args[0] );
					$this->class[] = $args[1];
				}
			break;


			
			case 3:
				$this->class[] = $args[0];
				$this->set_lines( $args[1] );
				$this->set_pages( $args[2] );
			break;


			
			default:
				throw new \InvalidArgumentException( "Invalid arguments." );
			break;
		}

		// Calls all automatic setting methods
		$this->callAllSetters();

		// Chainable
		return $this;
	}



	/**
	 * Hooks the notice into WP
	 */
	public function addAction()
	{
		global $pagenow;

		if( !empty( $this->lines ) and (empty($this->pages) or in_array( $pagenow, $this->pages)))
		{
			add_action( 'admin_notices', [ $this, 'render' ], $this->priority );
		}
	}






	/////////////////////////////////////////////
	// Setters
	/////////////////////////////////////////////



	/**
	 * Clear the content from the message
	 */
	public function clear()
	{
		$this->lines = [];
	}



	/**
	 * Set the content of the message.
	 * 
	 * @param array $lines 	A string or array of strings
	 */
	public function set_lines( $lines = [] )
	{
		$this->clear();

		$lines = array_filter( $this->makeArray( $lines ) );

		if( !empty( $lines ) )
		{
			$this->lines = $lines;
		}
	}



	/**
	 * Set the pages on which the notice should show.
	 * 
	 * @param array $lines 	A string or array of strings
	 */
	public function set_pages( $pages = [] )
	{
		$pages = array_filter( $this->makeArray( $pages ) );

		if( !empty( $pages ) )
		{
			$this->pages = $pages;
		}
	}



	public function add( $message = '' )
	{
		if( $this->nonEmptyString( $message ) )
		{
			$this->lines[] = $message;
		}
	}



	public function set_type( $type = '' )
	{
		if( $this->validType( $type ) )
		{
			$this->type = $type;
		}
	}



	public function set_dismissable( $dismissable = true )
	{
		$this->dismissable = boolval( $dismissable );
	}



	public function set_priority( $priority = 10 )
	{
		$this->priority = intval( $priority );
	}






	/////////////////////////////////////////////
	// Validation
	/////////////////////////////////////////////



	protected function nonEmptyString( $str = '' )
	{
		if( is_string( $str ) and !empty( $str ) )
		{
			return true;
		}

		return false;
	}



	public function validType( $type = '' )
	{
		return in_array( $type, $this->types_allowed );
	}







	/////////////////////////////////////////////
	// Rendering
	/////////////////////////////////////////////



	public function render( $output = true )
	{
		$html = Prefab::notice( $this->type )->addClass( $this->class );

		if( $this->dismissable )
		{
			$html->addClass( 'is-dismissible' );
		}

		$html->repeat( (new Tag('p'))->setEscContent( false ), $this->lines );

		if( $output === false )
		{
			return $html->render( false );
		}
		else
		{
			$html->render();
		}		
	}







	/////////////////////////////////////////////
	// Static single call methods
	/////////////////////////////////////////////
	


	public static function __callStatic( $name, $args )
	{
		$notice = new Notice;

		$name = strtolower( $name );

		if( $notice->validType( $name ) )
		{
			$notice->set_type( $name );

			call_user_func_array([ $notice, 'set' ], $args );

			$notice->addAction();

			return $notice;
		}

		throw new \BadMethodCallException( "Method '{$name}' does not exist." );
	}
}
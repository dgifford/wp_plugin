# A Framework for Wordpress Plugins #

Provides a framework for setting up a plugin. Includes classes for:

* Maintaining plugin information
* Storing plugin options
* Creating plugin settings pages
* Creating custom post types
* Creating shortcodes





## Future ##

## Classes ##

### Plugin ###

Creates the base plugin object.

### Notice ###

Generates a WordPress admin notice.
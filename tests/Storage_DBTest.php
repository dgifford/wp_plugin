<?php
Namespace dgifford\WP_Plugin;



/*
	Composer autoloader
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class Storage_DBTest extends \BW_UnitTestCase
{
	////////////////////////////////////////////////////
	// Setup and config
	////////////////////////////////////////////////////



	public function setUp()
	{
		$this->plugin = new Plugin( __DIR__ . '/test-plugin-in-folder/test-plugin-in-folder.php' );

		$this->storage = new Storage_DB( $this->plugin );
	}



	public function tearDown()
	{
		$this->storage->clear();

		$this->assertSame( 0, $this->storage->count() );
	}






	////////////////////////////////////////////////////
	// Tests
	////////////////////////////////////////////////////



	public function testExists()
	{		
		$this->assertFalse( $this->storage->exists( 'foo' ) );

		$this->storage->set( 'foo', 'foo' );

		$this->assertTrue( $this->storage->exists( 'foo' ) );
	}



	public function testSetGetCount()
	{		
		$this->storage->set( 'foo', 'foo' );

		$this->storage->set( 'bar', 'bar' );

		$this->assertSame( 2, $this->storage->count() );

		$this->storage->default( 'war', 'WAR' );

		$this->assertSame( 3, $this->storage->count() );

		$this->assertSame( 'foo', $this->storage->get( 'foo' ) );
	}



	public function testOverwrite()
	{		
		$this->storage->set( 'foo', 'foo' );

		$this->assertSame( 1, $this->storage->count() );

		$this->assertSame( 'foo', $this->storage->get( 'foo' ) );

		$this->storage->set( 'foo', 'bar' );

		$this->assertSame( 1, $this->storage->count() );

		$this->assertSame( 'bar', $this->storage->get( 'foo' ) );
	}



	public function testSetArray()
	{
		$this->storage->set([ 'woo' => 'woo', 'war' => 'war', ]);

		$this->assertSame( 2, $this->storage->count() );

		$this->assertSame( 'woo', $this->storage->get( 'woo' ) );
	}



	public function testSetArrayAppends()
	{
		$this->storage->set([ 'foo' => 'foo', 'bar' => 'bar', ]);

		$this->assertSame( 2, $this->storage->count() );

		$this->storage->set([ 'foo' => 'foobar', 'war' => 'war', ]);

		$this->assertSame( 3, $this->storage->count() );

		$this->assertSame( 'foobar', $this->storage->get( 'foo' ) );

		$this->assertSame( 'war', $this->storage->get( 'war' ) );
	}



	public function testDelete()
	{
		$this->storage->set([ 'foo' => 'foo', 'bar' => 'bar', ]);

		$this->storage->delete( 'bar' );

		$this->assertSame( 1, $this->storage->count() );
	}



	public function testClear()
	{
		$this->storage->set([ 'foo' => 'foo', 'bar' => 'bar', ]);

		$this->storage->default( 'war', 'WAR' );

		$this->storage->clear();

		$this->assertSame( 0, $this->storage->count() );
	}



	public function testClearDefaults()
	{
		$this->storage->set([ 'foo' => 'foo', 'bar' => 'bar', ]);

		$this->storage->default( 'war', 'WAR' );

		$this->storage->clearDefaults();

		$this->assertSame( 2, $this->storage->count() );
	}



	public function testGetAll()
	{
		$this->storage->set([ 'foo' => 'foo', 'bar' => 'bar', ]);

		$this->assertSame( $this->storage->getAll(),
		[
			'bar' => 'bar',
			'foo' => 'foo',
		]);
	}



	public function testGetKeysNoDefaults()
	{
		$this->storage->set([ 'foo' => 'FOO', 'bar' => 'BAR', 'woo' => 'WOO', ]);

		$this->assertSame( $this->storage->keys(), [ 'bar', 'foo', 'woo', ]);
	}



	public function testDefault()
	{
		$this->storage->default( 'war', 'war' );

		$this->assertSame( 'war', $this->storage->get( 'war' ) );

		$this->assertNull( $this->storage->get( 'foo' ) );
	}
	


	public function testHasDefault()
	{
		$this->storage->set([ 'foo' => 'FOO', 'bar' => 'BAR', 'woo' => 'WOO', ]);

		$this->assertFalse( $this->storage->hasDefault( 'woo' ) );

		$this->storage->default( 'woo', 'woo' );

		$this->assertTrue( $this->storage->hasDefault( 'woo' ) );
	}



	public function testDefaultArray()
	{
		$this->storage->default([ 'foo' => 'foo', 'bar' => 'bar', ]);

		$this->assertSame( 'foo', $this->storage->get( 'foo' ) );
	}



	public function testGetAllWithDefaults()
	{
		$this->storage->set([ 'foo' => 'foo', 'bar' => 'bar', ]);

		$this->storage->default([ 'foo' => 'FOO', 'woo' => 'woo', ]);

		$this->assertSame( 'woo', $this->storage->get( 'woo' ) );

		$this->assertSame( $this->storage->getAll(),
		[
			'foo' => 'foo',
			'woo' => 'woo',
			'bar' => 'bar',
		]);
	}



	public function testSetDefaultForExistingValue()
	{
		$this->storage->set([ 'foo' => 'foo', 'bar' => 'bar', ]);

		$this->storage->default( 'foo', 'WOO' );

		$this->assertSame( 'foo', $this->storage->get( 'foo' ) );

		$this->storage->delete( 'foo' );

		$this->assertSame( 'WOO', $this->storage->get( 'foo' ) );
	}



	public function testUninstall()
	{
		$this->storage->set([ 'foo' => 'foo', 'bar' => 'bar', ]);

		$this->assertSame( "delete_option( '{$this->plugin->prefix( 'bar' )}' );\ndelete_option( '{$this->plugin->prefix( 'foo' )}' );", $this->storage->uninstall() );
	}
}

# Manual Tests #

These tests are performed in the WP admin interface.

## Tests ##

### Required field ###

Submitting the settings with the 'Required' field empty results in the HTML5 'Please complete this field' error in the browser.

Remove the 'required' attribute in the browser. Submit with  'Required' field empty results in a WP error 'The field 'Required' is required.'


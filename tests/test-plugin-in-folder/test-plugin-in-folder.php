<?php
/*
Plugin Name: Test Plugin in Folder
Plugin URI: 
Description: Test WP_Plugin class
Version: 1.0
Author: Dan Gifford
Author URI: http://dancoded.com/


	Copyright (C) 2016  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



/*
	Prevent direct access
 */
if( !defined( 'ABSPATH' ) ) { exit; }



/*
	Composer autoloader
 */
require_once __DIR__ . '/../wp_plugin/vendor/autoload.php';




/*
	Classes used
 */
Use dgifford\WP_Plugin\Plugin;
Use dgifford\WP_Plugin\Settings_Page;



/*
	Initialise the plugin with the path to this file
 */
$plugin = new Plugin( __FILE__ );

$settings = new Settings_Page( $plugin );
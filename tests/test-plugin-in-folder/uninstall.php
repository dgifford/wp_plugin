<?php
/*
	Wordpress Plugin auto-generated uninstall script.


	Copyright (C) 2017  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



if( !defined( 'WP_UNINSTALL_PLUGIN' ) or !current_user_can( 'activate_plugins' ) )
{
	wp_die( __( 'You do not have sufficient permissions to access this page.' ), 403 );
}

delete_option( 'test_plugin_in_folder_plugin_state' );
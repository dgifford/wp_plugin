<?php
Namespace dgifford\WP_Plugin;



/*
	Composer autoloader
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class NoticeTest extends \BW_UnitTestCase
{
	////////////////////////////////////////////////////
	// Setup and config
	////////////////////////////////////////////////////








	////////////////////////////////////////////////////
	// Tests
	////////////////////////////////////////////////////



	public function testDefaultInfoNotice()
	{
		$notice = New Notice( 'FooBar' );

		$this->assertSame( '<div class="notice notice-info is-dismissible"><p>FooBar</p></div>', $notice->render( false ) );
	}



	public function testMultiLineInfoNotice()
	{
		$notice = New Notice([ 'line 1', 'line 2', 'line 3', ]);

		$this->assertSame( '<div class="notice notice-info is-dismissible"><p>line 1</p><p>line 2</p><p>line 3</p></div>', $notice->render( false ) );
	}



	public function testCustomClass()
	{
		$notice = New Notice( 'class_name', 'FooBar' );

		$this->assertSame( '<div class="notice notice-info class_name is-dismissible"><p>FooBar</p></div>', $notice->render( false ) );
	}


	public function testCustomClassNotDismissable()
	{
		$notice = New Notice( 'class_name', 'FooBar', false );

		$this->assertSame( '<div class="notice notice-info class_name"><p>FooBar</p></div>', $notice->render( false ) );
	}



	public function testInvalidStaticCall()
	{
		$this->expectException( \BadMethodCallException::class );

		Notice::yoyo('arse');
	}



	public function testError()
	{
		$notice = Notice::error('FooBar');

		$this->assertSame( '<div class="notice notice-error is-dismissible"><p>FooBar</p></div>', $notice->render( false ) );
	}

}

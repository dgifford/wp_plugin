<?php
Namespace dgifford\WP_Plugin;



/*
	Composer autoloader
 */
require_once(__DIR__ . '/../vendor/autoload.php');



Use dgifford\Tag\Tag;
Use dgifford\html\Prefab;



class SettingsPageTest extends \BW_UnitTestCase
{
	////////////////////////////////////////////////////
	// Setup and config
	////////////////////////////////////////////////////



	public static function setUpBeforeClass()
	{
		// Move example plugin files
		shell_exec('cp -r ' . __DIR__ . '/test-plugin-in-folder ' . __DIR__ . '/../../test-plugin-in-folder');
		shell_exec('cp ' . __DIR__ . '/test-plugin.php ' . __DIR__ . '/../../test-plugin.php');
	}



	public static function tearDownAfterClass()
	{
		// Remove example plugin files
		shell_exec('rm -r ' . __DIR__ . '/../../test-plugin-in-folder');
		shell_exec('rm ' . __DIR__ . '/../../test-plugin.php');
	}



	public function setUp()
	{
		$this->plugin_file = realpath(__DIR__ . '/../../test-plugin-in-folder/test-plugin-in-folder.php');

		$this->plugin = new Plugin( $this->plugin_file );

		$this->settings_page = new Settings_Page( $this->plugin );
	}



	public function tearDown()
	{
		$this->settings_page->storage->clear();

		$this->settings_page->clearErrors();
	}






	////////////////////////////////////////////////////
	// Utility functions
	////////////////////////////////////////////////////



	public function testGetFieldArgumentsFromLabel()
	{
		$properties = $this->settings_page->getFieldArguments( 'Foo bar' );

		$this->assertEquals(
		[
			'type' => 'text',
			'label' => 'Foo bar',
			'description' => '',
			'default' => null,
			'attributes' => 
			[
				'name' 		=> 'Foo_bar',
				'id' 		=> 'Foo_bar-id',
			],
			'children' => [],
		]
		, $properties );
	}



	public function testGetFieldArgumentsFromTypeAndLabel()
	{
		$properties = $this->settings_page->getFieldArguments( 'number', 'Foo bar' );

		$this->assertEquals(
		[
			'type' => 'number',
			'label' => 'Foo bar',
			'description' => '',
			'default' => null,
			'attributes' => 
			[
				'name' 		=> 'Foo_bar',
				'id' 		=> 'Foo_bar-id',
			],
			'children' => [],
		]
		, $properties );
	}



	public function testGetFieldArgumentsFromTypeLabelAndProperties()
	{
		$properties = $this->settings_page->getFieldArguments( 'number', 'Foo bar', 
		[
			'description' => 'description',
			'default' => 'yo',
			'attributes' => 
			[
				'name' 		=> 'my_name',
				'class' 	=> 'my_class',
			],
		]);

		$this->assertEquals(
		[
			'type' => 'number',
			'label' => 'Foo bar',
			'description' => 'description',
			'default' => 'yo',
			'attributes' => 
			[
				'name' 		=> 'my_name',
				'class' 	=> 'my_class',
				'id' 		=> 'my_name-id',
			],
			'children' => [],
		]
		, $properties );
	}



	public function testPageHTMLNoFields()
	{
		$this->assertEquals( '<div class="wrap test-plugin-in-folder"><h1>Test Plugin in Folder Settings</h1><form action="admin-post.php" method="POST"><input name="_wp_http_referer" type="hidden" value="/"/><input name="action" type="hidden" value="test-plugin-in-folder"/><input id="_wpnonce" name="_wpnonce" type="hidden" value="' . wp_create_nonce( $this->settings_page->menu_slug ) . '"/><table class="form-table"></table><button class="button button-primary" id="submit" name="submit" type="submit">Save Settings</button></form></div>', $this->settings_page->render( false ) );
	}






	////////////////////////////////////////////////////
	// Adding fields
	////////////////////////////////////////////////////



	public function testAddTagObject()
	{
		$this->settings_page->add( new Tag( 'tr', 'foobar' ) );

		$this->assertSame(
			'<table class="form-table"><tr>foobar</tr></table>', 
			$this->settings_page->html->find('table')[0]->render( false ) 
		);
	}



	public function testAddDefaultControl()
	{
		$this->settings_page->add( 'Foo bar' );

		$this->assertSame( 
			'<table class="form-table"><tr><th scope="row"><label for="Foo_bar-id">Foo bar</label></th><td><input class="regular-text" id="Foo_bar-id" name="Foo_bar" type="text"/></td></tr></table>', 
			$this->settings_page->html->find('table')[0]->render( false ) 
		);
	}



	public function testAddDefaultControlWithProperties()
	{
		$this->settings_page->add( 'Foo bar', [ 'label' => 'Foo Bar Label', 'description' => 'A description of the field.'] );

		$this->assertSame( 
			'<table class="form-table"><tr><th scope="row"><label for="Foo_bar-id">Foo Bar Label</label></th><td><input aria-describedby="Foo_bar-description" class="regular-text" id="Foo_bar-id" name="Foo_bar" type="text"/><p class="description" id="Foo_bar-description">A description of the field.</p></td></tr></table>', 
			$this->settings_page->html->find('table')[0]->render( false ) 
		);
	}



	public function testAddDefaultControlWithAttribute()
	{
		$this->settings_page->add( 'Foo bar', [ 'attributes' => [ 'class' => 'my_class'] ] );

		$this->assertSame( 
			'<table class="form-table"><tr><th scope="row"><label for="Foo_bar-id">Foo bar</label></th><td><input class="my_class" id="Foo_bar-id" name="Foo_bar" type="text"/></td></tr></table>', 
			$this->settings_page->html->find('table')[0]->render( false ) 
		);
	}



	public function testAddDefaultControlWithAttributes()
	{
		$this->settings_page->add( 'Foo bar', [ 'attributes' => [ 'class' => 'my_class', 'value' => 'woo'] ] );

		$this->assertSame( 
			'<table class="form-table"><tr><th scope="row"><label for="Foo_bar-id">Foo bar</label></th><td><input class="my_class" id="Foo_bar-id" name="Foo_bar" type="text" value="woo"/></td></tr></table>', 
			$this->settings_page->html->find('table')[0]->render( false ) 
		);
	}



	public function testAddTextControlWithProperties()
	{
		$this->settings_page->add( 'text', 'Foo bar', [ 'label' => 'Foo Bar Label', 'description' => 'A description of the field.'] );

		$this->assertSame( 
			'<table class="form-table"><tr><th scope="row"><label for="Foo_bar-id">Foo Bar Label</label></th><td><input aria-describedby="Foo_bar-description" class="regular-text" id="Foo_bar-id" name="Foo_bar" type="text"/><p class="description" id="Foo_bar-description">A description of the field.</p></td></tr></table>', 
			$this->settings_page->html->find('table')[0]->render( false ) 
		);
	}



	public function testAddControlWithDefaultValue()
	{
		$this->settings_page->add( 'Default', [ 'default' => 'Foo', ] );

		// Value not set yet
		$this->assertSame( 
			'<table class="form-table"><tr><th scope="row"><label for="Default-id">Default</label></th><td><input class="regular-text" id="Default-id" name="Default" type="text"/></td></tr></table>', 
			$this->settings_page->html->find('table')[0]->render( false ) 
		);

		// Values only added when form rendered
		$this->assertInternalType( 'int', strpos( $this->settings_page->render( false ), '<input class="regular-text" id="Default-id" name="Default" type="text" value="Foo"/>') );

	}



	public function testAddControlForExistingStoredValue()
	{
		$this->settings_page->storage->set( 'test', 'bar' );

		$this->settings_page->add( 'text', 'test', [ 'label' => 'test Label' ] );

		// Existing value not added yet
		$this->assertInternalType( 'int', strpos( $this->settings_page->render( false ), '<input class="regular-text" id="test-id" name="test" type="text" value="bar"/>') );

		// Values only added when form rendered
		$this->assertInternalType( 'int', strpos( $this->settings_page->render( false ), '<input class="regular-text" id="test-id" name="test" type="text" value="bar"/>') );
	}



	public function testFieldAddedAutomaticallyForStoredValue()
	{
		$this->settings_page->storage->set( 'Foo', 'bar' );

		$this->assertInternalType( 'int', strpos( $this->settings_page->render( false ), '<input class="regular-text" id="Foo-id" name="Foo" type="text" value="bar"/>') );
	}



	public function testBooleanField()
	{
		$this->settings_page->add( 'boolean', 'Foo bar' );

		$this->assertSame( 
			'<table class="form-table"><tr><th scope="row"><label for="Foo_bar-id">Foo bar</label></th><td><input name="Foo_bar" type="hidden" value="0"/><input id="Foo_bar-id" name="Foo_bar" type="checkbox" value="1"/></td></tr></table>', 
			$this->settings_page->html->find('table')[0]->render( false ) 
		);
	}



	public function testSelectField()
	{
		$this->settings_page->add( 'select', 'Foo bar', [ 'children' => [ 'Option 1', 'Option 2', 'Option 3' ] ] );

		$this->assertSame( 
			'<select id="Foo_bar-id" name="Foo_bar"><option>Option 1</option><option>Option 2</option><option>Option 3</option></select>', 
			$this->settings_page->html->find('select')[0]->render( false ) 
		);
	}



	public function testSelectFieldWithAttributes()
	{
		$this->settings_page->add( 'select', 'Foo bar', ['attributes' => [ 'class' => 'my_class' ], 'children' => [ 'Option 1', 'Option 2', 'Option 3' ] ] );

		$this->assertSame( 
			'<select class="my_class" id="Foo_bar-id" name="Foo_bar"><option>Option 1</option><option>Option 2</option><option>Option 3</option></select>', 
			$this->settings_page->html->find('select')[0]->render( false ) 
		);
	}



	public function testTextareaField()
	{
		$this->settings_page->add( 'textarea', 'Foo bar' );

		$this->assertSame( 
			'<table class="form-table"><tr><th scope="row"><label for="Foo_bar-id">Foo bar</label></th><td><textarea class="large-text" id="Foo_bar-id" name="Foo_bar"></textarea></td></tr></table>', 
			$this->settings_page->html->find('table')[0]->render( false ) 
		);
	}






	////////////////////////////////////////////////////
	// Input sanitization
	////////////////////////////////////////////////////



	public function testDefaultSantizeWithValidText()
	{
		$this->settings_page->add( 'Foo bar' );

		$this->assertSame( 'valid text', $this->settings_page->sanitize( 'Foo_bar', 'valid text' ) );
	}



	public function testDefaultSantizeWithInvalidTextWithTags()
	{
		$this->settings_page->add( 'Foo bar' );

		$this->assertSame( 'invalid textalert(&#34;yo&#34;)', $this->settings_page->sanitize('Foo_bar', 'invalid text<script>alert("yo")</script>' ) );
	}



	public function testSanitizeInvalidNumberField()
	{
		$this->settings_page->add( 'number', 'Foo number' );

		$this->assertSame( 0, $this->settings_page->sanitize('Foo_number', 'invalid text' ) );
	}



	public function testSanitizeValidNumberField()
	{
		$this->settings_page->add( 'number', 'Foo bar' );

		$this->assertSame( 100, $this->settings_page->sanitize('Foo_bar', '100') );
	}



	public function testAddRequiredControl()
	{
		$this->settings_page->add( 'text', 'Foo bar', [ 'attributes' => [ 'required' => false, ] ] );

		$this->settings_page->add( 'boolean', 'Woo', [ 'attributes' => [ 'required' => true, ] ] );

		$this->assertSame( 
			'<input name="Woo" type="hidden" value="0"/><input id="Woo-id" name="Woo" required="required" type="checkbox" value="1"/>', 
			$this->settings_page->html->find('input', ['name' => 'Woo', 'type' => 'checkbox', ])[0]->render( false ) 
		);

		$this->assertFalse( $this->settings_page->isFieldRequired( 'Foo_bar' ) );

		$this->assertTrue( $this->settings_page->isFieldRequired( 'Woo' ) );
	}






	////////////////////////////////////////////////////
	// Input validation
	////////////////////////////////////////////////////



	public function testDefaultValidateWithValidText()
	{
		$this->settings_page->add( 'Foo bar' );

		$this->assertTrue( $this->settings_page->validate('Foo_bar', 'valid text') );
	}



	public function testDefaultValidateWithInvalidValue()
	{
		$this->settings_page->add( 'Foo text' );

		$this->settings_page->validate('Foo_text', false );

		$this->assertFalse( $this->settings_page->validator->isValid() );
	}



	public function testValidateInvalidNumberField()
	{
		$this->settings_page->add( 'number', 'Foo number' );

		$this->assertSame( 
			'<table class="form-table"><tr><th scope="row"><label for="Foo_number-id">Foo number</label></th><td><input class="small-text" id="Foo_number-id" name="Foo_number" type="number"/></td></tr></table>', 
			$this->settings_page->html->find('table')[0]->render( false ) 
		);

		$this->settings_page->validate('Foo_number', 'invalid' );

		$this->assertFalse( $this->settings_page->validator->isValid() );

	}



	public function testValidateValidNumberField()
	{
		$this->settings_page->add( 'number', 'Foo bar' );

		$this->assertTrue( $this->settings_page->validate('Foo_bar', '100' ) );
	}






	////////////////////////////////////////////////////
	// Errors
	////////////////////////////////////////////////////



	public function testErrorsGenerated()
	{
		$this->settings_page->add( 'text', 'Foo bar', [ 'attributes' => [ 'required' => true, ] ] );

		$this->assertFalse( $this->settings_page->validate( 'Foo_bar', '' ) );
		
		$this->assertSame( [$this->settings_page->getRequiredErrorMessage( 'Foo_bar' )], $this->settings_page->getError( 'Foo_bar' ) );
	}



	public function testErrorsPersisted()
	{
		$this->settings_page->add( 'text', 'Foo bar', [ 'attributes' => [ 'required' => true, ] ] );

		$this->assertFalse( $this->settings_page->validate( 'Foo_bar', '' ) );

		//$this->settings_page->saveErrors();

		$settings_page = new Settings_Page( $this->plugin );
		
		$this->assertSame( [$this->settings_page->getRequiredErrorMessage( 'Foo_bar' )], $settings_page->getError( 'Foo_bar' ) );

		$this->assertSame( '&status=errors', substr($settings_page->getRedirectURL(), -14 ) );
	}

}

<?php
Namespace dgifford\WP_Plugin;



/*
	Composer autoloader
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class PluginTest extends \BW_UnitTestCase
{
	////////////////////////////////////////////////////
	// Setup and config
	////////////////////////////////////////////////////



	public static function setUpBeforeClass()
	{
		// Move example plugin files
		shell_exec('cp -r ' . __DIR__ . '/test-plugin-in-folder ' . __DIR__ . '/../../test-plugin-in-folder');
		shell_exec('cp ' . __DIR__ . '/test-plugin.php ' . __DIR__ . '/../../test-plugin.php');
	}



	public static function tearDownAfterClass()
	{
		// Remove example plugin files
		shell_exec('rm -r ' . __DIR__ . '/../../test-plugin-in-folder');
		shell_exec('rm ' . __DIR__ . '/../../test-plugin.php');
	}



	public function setUp()
	{
		$this->plugin_file = realpath(__DIR__ . '/../../test-plugin-in-folder/test-plugin-in-folder.php');

		$this->plugin = new Plugin( $this->plugin_file );
	}






	////////////////////////////////////////////////////
	// Tests
	////////////////////////////////////////////////////



	public function testFilePath()
	{
		$this->assertSame( $this->plugin_file, $this->plugin->file() );
	}



	public function testName()
	{
		$this->assertSame( 'Test Plugin in Folder', $this->plugin->name() );
	}



	public function testCreatePluginWithInvalidFilePath()
	{
		$this->expectException( \InvalidArgumentException::class );

		$plugin = new Plugin( 'foobar' );
	}



	public function testActivatePlugin()
	{
		activate_plugin( $this->plugin_file );

		$this->assertSame( 'activated', $this->plugin->options->get('plugin_state') );
	}
}

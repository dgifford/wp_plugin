<?php
Namespace dgifford\WP_Plugin;



/*
	Composer autoloader
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class ShortcodeTest extends \BW_UnitTestCase
{
	////////////////////////////////////////////////////
	// Setup and config
	////////////////////////////////////////////////////



	public static function setUpBeforeClass()
	{
		// Move example plugin files
		shell_exec('cp -r ' . __DIR__ . '/test-plugin-in-folder ' . __DIR__ . '/../../test-plugin-in-folder');
		shell_exec('cp ' . __DIR__ . '/test-plugin.php ' . __DIR__ . '/../../test-plugin.php');
	}



	public static function tearDownAfterClass()
	{
		// Remove example plugin files
		shell_exec('rm -r ' . __DIR__ . '/../../test-plugin-in-folder');
		shell_exec('rm ' . __DIR__ . '/../../test-plugin.php');
	}



	public function setUp()
	{
		$this->plugin_file = realpath(__DIR__ . '/../../test-plugin-in-folder/test-plugin-in-folder.php');

		$this->plugin = new Plugin( $this->plugin_file );
	}






	////////////////////////////////////////////////////
	// Tests
	////////////////////////////////////////////////////



	public function testShortcodeRegistered()
	{
		$shortcode = new basic_shortcode( $this->plugin );

		$this->assertSame( 'basic_shortcode', $shortcode->tag );
	}



	public function testDefaultShortcodeOutput()
	{
		$shortcode = new basic_shortcode( $this->plugin );

		$this->assertSame( 'output name: number: content:', do_shortcode('[basic_shortcode]') );
	}



	public function testShortcodeWithAttribute()
	{
		$shortcode = new basic_shortcode( $this->plugin );

		$this->assertSame( 'output name:foo number: content:', do_shortcode('[basic_shortcode name="foo"]') );
	}



	public function testShortcodeWithContent()
	{
		$shortcode = new basic_shortcode( $this->plugin );

		$this->assertSame( 'output name: number: content:foo', do_shortcode('[basic_shortcode]foo[/basic_shortcode]') );
	}



	public function testShortcodeWithContentAndAttribute()
	{
		$shortcode = new basic_shortcode( $this->plugin );

		$this->assertSame( 'output name:foo number: content:bar', do_shortcode('[basic_shortcode name="foo"]bar[/basic_shortcode]') );
	}



	public function testShortcodeNumberAttributeWithValidation()
	{
		$shortcode = new basic_shortcode( $this->plugin );

		$this->assertSame( 'output name: number:foo content:', do_shortcode('[basic_shortcode number="foo"]') );

		$shortcode->set_validation_filters(['number' => 'number']);

		$this->assertSame( 'output name: number: content:', do_shortcode('[basic_shortcode number="foo"]') );
		
		$this->assertSame( 'output name: number:123 content:', do_shortcode('[basic_shortcode number="123"]') );
	}


}



class basic_shortcode extends Shortcode
{
	// Default attributes
	public $defaults =
	[
		'name' => '',
		'number' => '',
	];



	public function action()
	{
		$atts = '';

		return "output {$this->getAttributes()} content:{$this->content}";
	}



	public function getAttributes()
	{
		$result = [];

		foreach( $this->attributes as $name => $value )
		{
			$result[] = "{$name}:{$value}";
		}

		return implode(' ', $result);
	}

}

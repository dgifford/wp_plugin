<?php
Namespace dgifford\WP_Plugin;



/*
	Composer autoloader
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class UninstallerTest extends \BW_UnitTestCase
{
	////////////////////////////////////////////////////
	// Setup and config
	////////////////////////////////////////////////////



	public function setUp()
	{
		$this->plugin = new Plugin( __DIR__ . '/test-plugin-in-folder/test-plugin-in-folder.php' );
	}






	////////////////////////////////////////////////////
	// Tests
	////////////////////////////////////////////////////



	public function testNotExists()
	{		
		$this->assertNull( $this->plugin->uninstaller );
	}



	public function testExists()
	{
		$this->plugin->onActivation();
		
		$this->assertInstanceOf( Uninstaller::class, $this->plugin->uninstaller );
	}

}

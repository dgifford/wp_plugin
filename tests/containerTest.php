<?php
/*
	Copyright (C) 2016  Dan Gifford

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



/*
	Composer autoloader
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class containerTest extends \BW_UnitTestCase
{
	public function testCreate()
	{
		$plugin = new Plugin( 'Test_plugin' );

		$this->assertSame( 'Test_plugin_FooBar', $plugin->info->prefix( 'FooBar' ) );

		$plugin->options->set( 'foo', 'bar' );

		$this->assertSame( ['Test_plugin_foo' => 'bar' ], $plugin->options->data );
	}
}


class Plugin
{
	public function __construct( $name = '' )
	{
		$this->name = $name;

		$this->info = New Info( $this );

		$this->options = New Options( $this );
	}
}


class Info
{
	public function __construct( Plugin $plugin )
	{
		$this->plugin = $plugin;
	}


	public function prefix( $string = '' )
	{
		return $this->plugin->name . '_' . $string;
	}
}



class Options
{
	public $data = [];



	public function __construct( Plugin $plugin )
	{
		$this->plugin = $plugin;
	}



	public function set( $name, $value = 1 )
	{
		$this->data[ $this->plugin->info->prefix( $name ) ] = $value;
	}
}
